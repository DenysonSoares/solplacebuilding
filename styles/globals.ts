import { createGlobalStyle } from 'styled-components'
import { Colors, Paddings, Fonts } from './variables'

export const GlobalStyle = createGlobalStyle`
  *,
  body,
  html {
    margin: 0;
    padding: 0;
    font-family: "Roboto", sans-serif;
    position: relative;
    box-sizing: border-box;
    font-size: 100%;
  }
  body {
    background-color: #e5e5e5;
  }
 

  /* FONTES */
  h1{
    font-size: ${Fonts.TitlePrincipal};
    strong{
      color: ${Colors.orangeDefault};
      text-transform: capitalize;
    }
  }
  h2{
    font-size: ${Fonts.TitlePrincipal};
    color: ${Colors.orangeDefault};
  }
  h3{
    font-size: ${Fonts.TitleSecondary};
    color: ${Colors.orangeDefault};
  }
  p, label{
    font-size: ${Fonts.Paragrapher};
  }
  a, button{
    text-decoration: none;
    cursor: pointer;
  }

  /* FORMULARIOS */
  input[type="text"], select {
    outline: none;
    padding-left: 20px;
    font-size: 16px;
    margin-top: 5px;
    ::placeholder {
      font-size: 14px;
      color: rbga(0,0,0,0.1);
    }
  }
  input[type="text"]:focus, select:focus {
    border-color: #061887;
  }
   input[type="radio"]{
     width:auto;
   }
  form{
    button{
      width: 100%;
    }
    label{
      color: #575757;
      font-weight: 500;
      font-size: ${Fonts.Paragrapher};
      position: relative;
      display: block;
      span{
        color: #c80000;
        font-size: 14px;
        display: block;
        margin-top: -20px;
        margin-bottom: 20px;
      }
      svg{
        position: absolute;
        top: 40px;
        right: 20px;
      }
      input{
        width: 100%;
        height: 50px;
        background: #FEFEFE;
        border: 1px solid #B6B6B6;
        box-sizing: border-box;
        border-radius: 5px;
        margin-bottom: 25px;
      }
      select{
        width: 100%;
        height: 50px;
        background: #FEFEFE;
        border: 1px solid #B6B6B6;
        box-sizing: border-box;
        border-radius: 5px;
        margin-bottom: 25px;
      }
    }
    .rangeContent{
      display: grid;
      grid-template-columns: 9fr 3fr;
      border: 1px solid #B6B6B6;
      border-radius: 10px;
      padding: 15px;
      margin-bottom: 25px;
      .rangeBox{
        display: flex;
        align-items: center;
        justify-content: center;
        grid-gap: 10px;
        padding: 0px 20px;
        input{
          margin: 0;
          margin-bottom: 3px;
          color: red;
          border: 0;
        }
        p{
          position: absolute;
          left: 50%;
          top: -7px;
          font-size: 14px;
          background-color: ${Colors.blueDefault};
          padding: 5px;
          border-radius: 5px;
          color: #ffffff;
        }
      }
    }
  }
  section{
    display: grid;
    grid-template-columns: 2fr 1fr 1fr;
    align-items: center;
    justify-content:space-between;
    padding: 10px 0px;
    border-top: 1px solid #A2A2A2;
    color: #575757;
    &:first-child{
      border: 0;
    }
    &:last-child{
      margin-bottom: 20px;
    }
    input[type=file]{
      visibility: hidden;
      height: 40px;
      overflow: inherit;
      float: right;
      &:placeholder-shown {
        background-color:red;
      }
      :before{
        visibility: visible;
        content: "Enviar +";
        font-weight: 600;
        color: #061887;
        background: #FEFEFE;
        border: 1px solid #061887;
        box-sizing: border-box;
        box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
        padding: 10px 30px;
        z-index: 1;
        float: right;
      }
    }
  }

  /* MODAL STYLES */
  .react-modal-overlay {
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.5);
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 9;
  }
  .react-modal-content {
    background: #fefefe;
    border-radius: 8px;
    max-width: 688px;
    padding: 40px;
    position: relative;
  }
  .react-close-modal {
    position: absolute;
    right: 1rem;
    top: 0.5rem;
    border: 0;
    background-color: transparent;
    font-size: 1.6rem;
  }

  .backButton{
    background-color: #FEFEFE;
    padding: 10px 20px;
    border: 0.5px solid rgba(87, 87, 87, 0.5);
    box-sizing: border-box;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    font-size: 16px;
    color: #232323;
    font-weight: 500;
    margin-right: 10px;
  }
  .twoForms{
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 20px;
  }
  .treeForms{
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 20px;
  }


  @media(max-width: 1440px) {
    body{
      font-size: 80%;
    }
  }
`
