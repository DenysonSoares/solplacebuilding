import styled from "styled-components";

// Colors
export const Colors = {
  blueDefault: "#061887",
  orangeDefault: "#FF8500",
  bodyBackgroundDefault: "#E5E5E5",
  colorFont: "#575757",
};

export const Paddings = {
  paddingContainer: "3% 6%",
  paddingSides: "0% 6%",
};

export const Fonts = {
  TitlePrincipal: "2em",
  TitleSeconrary: "1.375em",
  Paragrapher: "1.125em",
};
