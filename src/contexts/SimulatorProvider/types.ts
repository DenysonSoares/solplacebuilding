import { ReactNode } from 'react';

export interface Simulator{
  name: string,
  email: string,
  telefone: number,
  preco: number,
  entrada: number,
  tipo_cliente: string,
  numero_parcela_sem_garantia: number,
  numero_parcela_veiculo: number,
  numero_parcela_imovel: number,
  tipo_conexao: string,
  consumo_medio: number,
  dimensionado: number,
  state: string,
  estado: string,
  partner_id: string,
}

export type SimulatorInput = Omit<Simulator, 'id' | 'createdAt'>;

export interface SimulatorsProviderProps{
  children: ReactNode;
}

export interface SimulatorsContextData {
  simulators: Simulator[];
  createSimulator: (simulators: SimulatorInput) => Promise<void>;
}