import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { Simulator, SimulatorInput, SimulatorsContextData, SimulatorsProviderProps } from './types';

export const SimulatorsContext = createContext<SimulatorsContextData>(
  {} as SimulatorsContextData
);

export function SimulatorsProvider({children}: SimulatorsProviderProps) {
  const [simulators, setSimulators] = useState<Simulator[]>([]);

  async function createSimulator(simulatorsInput: SimulatorInput){
    const response = await api.post('/solplace/simulation/create', {
      params:{
        ...simulatorsInput
      } 
    })
    const { simulator } = response.data;

    setSimulators([
      ...simulators,
      simulator,
    ])
  }

  return(
    <SimulatorsContext.Provider value={{simulators, createSimulator}}>
      {children}
    </SimulatorsContext.Provider>
  );

}