import { AuthProvider } from "./AuthProvider/AuthContext";
import { FisicPersonProvider } from "./FisicPersonProvider/FisicPersonContext";
import { IntegratorsProvider } from "./IntegratorProvider/IntegratorContext";
import { JuridicPersonProvider } from "./JuridicPersonProvider/JuridicPersonContext";
import { PartnersProvider } from "./PartnerProvider/PartnersContext";
import { BasicProjectsProvider } from "./ProjetcsProvider/ProjectContext";
import { SimulatorsProvider } from "./SimulatorProvider/SimulatorContext";
import { NewPasswordsProvider } from "./UpdatePasswordContext/UpdatePasswordContext";
import { UsersProvider } from "./UserProvider/UsersContext";


export const AppProvider = ({ children }) => {
  return(
    <AuthProvider>
      <UsersProvider>
        <BasicProjectsProvider>
          <SimulatorsProvider>
            <IntegratorsProvider>
              <NewPasswordsProvider>
                <FisicPersonProvider>
                  <JuridicPersonProvider>
                    <PartnersProvider>
                      {children}
                    </PartnersProvider>
                  </JuridicPersonProvider>
                </FisicPersonProvider>
              </NewPasswordsProvider>
            </IntegratorsProvider>
          </SimulatorsProvider>
        </BasicProjectsProvider>
      </UsersProvider>
    </AuthProvider>
  );
}