import { ReactNode } from "react";

export interface User{
  email: string,
  password: string,
  name: string,
  telefone: number,
  cargo: string
  admin_id: number,
}

export type UserInput = Omit<User, 'id' | 'createdAt'>;

export interface UsersProviderProps{
  children: ReactNode;
}

export interface UsersContextData {
  users: User[];
  createUser: (users: UserInput) => Promise<void>;
}