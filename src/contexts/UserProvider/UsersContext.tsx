import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { User, UserInput, UsersContextData, UsersProviderProps } from './types';
import { useAlert } from 'react-alert'


export const UsersContext = createContext<UsersContextData>(
  {} as UsersContextData
);

export function UsersProvider({children}: UsersProviderProps) {
  const warning = useAlert()
  const [users, setUsers] = useState<User[]>([]);

  async function createUser(usersInput: UserInput){
    try {
      const { data } = await api.post('/solplace/user/signup', {
        params:{
          ...usersInput
        } 
      })

      if(data.result?.ok){
        const { user } = data;
        setUsers([
          ...users,
          user,
        ])
        warning.success("Usuário cadastrado com sucesso!");
      }else{
        warning.error(data.result.error);
      } 
    } catch (error) {
      return error;
    }
  }

  return(
    <UsersContext.Provider value={{users, createUser}}>
      {children}
    </UsersContext.Provider>
  );

}