import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { FisicPerson, FisicPersonInput, FisicPersonContextData, FisicPersonProviderProps } from './types';



export const FisicPersonContext = createContext<FisicPersonContextData>(
  {} as FisicPersonContextData
);

export function FisicPersonProvider({children}: FisicPersonProviderProps) {
  const [fisicPersons, setFisicPersons] = useState<FisicPerson[]>([]);

  async function createFisicPerson(fisicPersonsInput: FisicPersonInput){
    const { data } = await api.post('/solplace/client/create_pf', {
      params:{
        ...fisicPersonsInput
      } 
    })
    const { user } = data;
    console.log(data);
    setFisicPersons([
      ...fisicPersons,
      user,
    ])
  }

  return(
    <FisicPersonContext.Provider value={{fisicPersons, createFisicPerson}}>
      {children}
    </FisicPersonContext.Provider>
  );

}