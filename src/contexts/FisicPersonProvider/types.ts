import { ReactNode } from "react";

export interface FisicPerson{
  name: string, 
  data_nascimento: string, 
  email: string,
  phone: number,
  zip: string, 
  street: string, 
  l10n_br_number: number,                                
  l10n_br_district: string, 
  street2: string,
  state_id: string,
  city_id: string, 
  l10n_br_cnpj_cpf: string,                      
  nacionalidade: string,
  genero: string,
  naturalidade: string,
  escolaridade: string,
  nome_mae: string,
  estado_civil: string,
  nome_conjuge: string,
  cpf_conjuge: string,
  data_nasc_conjuge: string,
  orgao_emissor: string,
  rg: string,
  data_emissao: string, 
  uf_emissor: string,
  ocupacao: string,
  cargo: string,
  profissao: string,
  renda_mensal: number,
  patrimonio: number,
  company_type: string
}

export type FisicPersonInput = Omit<FisicPerson, 'id' | 'createdAt'>;

export interface FisicPersonProviderProps{
  children: ReactNode;
}

export interface FisicPersonContextData {
  fisicPersons: FisicPerson[];
  createFisicPerson: (fisicPerson: FisicPersonInput) => Promise<void>;
}