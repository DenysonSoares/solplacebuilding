import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { JuridicPerson, JuridicPersonInput, JuridicPersonContextData, JuridicPersonProviderProps } from './types';



export const JuridicPersonContext = createContext<JuridicPersonContextData>(
  {} as JuridicPersonContextData
);

export function JuridicPersonProvider({children}: JuridicPersonProviderProps) {
  const [juridicPersons, setJuridicPersons] = useState<JuridicPerson[]>([]);

  async function createJuridicPerson(juridicPersonsInput: JuridicPersonInput){
    const { data } = await api.post('/solplace/client/create_pj', {
      params:{
        ...juridicPersonsInput
      } 
    })
    const { juridicPerson } = data;
    console.log(data);
    setJuridicPersons([
      ...juridicPersons,
      juridicPerson,
    ])
  }

  return(
    <JuridicPersonContext.Provider value={{juridicPersons, createJuridicPerson}}>
      {children}
    </JuridicPersonContext.Provider>
  );

}