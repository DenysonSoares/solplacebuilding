import { ReactNode } from "react";

export interface JuridicPerson{
  name: string;
  email: string;
  phone: number;
  zip: string;
  street: string;
  l10n_br_number: number;
  l10n_br_district: string;
  street2: string;
  state_id: string;
  city_id: string;
  l10n_br_cnpj_cpf: string;
  company_type: string;
  l10n_br_legal_name: string;
  data_fundacao: string;
  natureza_legal: string;
  tamanho_empresa: string;
  numero_func: number;
  alteracoes_contrato: number;
  website: string;
  l10n_br_inscr_est: string;
  l10n_br_inscr_mun: string;
  faturamento_anual: number;
  atividade_economica: string;
}

export type JuridicPersonInput = Omit<JuridicPerson, 'id' | 'createdAt'>;

export interface JuridicPersonProviderProps{
  children: ReactNode;
}

export interface JuridicPersonContextData {
  juridicPersons: JuridicPerson[];
  createJuridicPerson: (juridicPerson: JuridicPersonInput) => Promise<void>;
}