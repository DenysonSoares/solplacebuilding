import router from "next/router"
import React, { createContext, useContext, useEffect, useState } from "react"
import api from "../../services/api"
import { IUser, IContext, IAuthProvider } from "./types"
import { getUserLocalStorage, setUserLocalStorage } from "./util"
import { setCookie } from "nookies"
import { useAlert } from 'react-alert'

export const AuthContext = createContext({} as IContext)

export const AuthProvider = ({children}: IAuthProvider) => {
  const warning = useAlert()
  const [user, setUser] = useState<IUser | null>();

  useEffect( () => {
    const user = getUserLocalStorage();

    if(user){
      setUser(user)
    }
  }, [])
  async function authenticate (user: string, password: string){
  
    try {
      const token = ""
      const { data } = await api.post('/solplace/user/login',{params:{user, password}})

      if (data.result.code === "001") {
        const payload = {
          user: data.result.email,
          id: data.result.id, 
          name: data.result.name, 
          email: data.result.email,
          admin_id: data.result.admin_id, 
          admin: data.result.admin,
          numero_projetos: data.result.numero_projetos
        }
        setUser(payload);
        setUserLocalStorage(payload);     
        warning.success("Login válido! Bem vindo!")

        setCookie(undefined, 'nextauth.token', token, {
          maxAge: 60 * 60 * 1, // 1 hour
        })
        
        router.push('/home')
      }else{
        //alert(data.result.error);
        warning.error("Usuário ou senha inválida")
      } 
    } catch (error) {
      return error;
    }

  }

  function logout() {
    setUser(null);
    setUserLocalStorage(null);
  }

  return(
    <AuthContext.Provider value={{...user, authenticate, logout}}>
      {children}
    </AuthContext.Provider>
  )
}
