export interface IUser {
  user: string
  id: number,
  name: string,
  email: string,
  admin_id: number,
  admin: number,
  numero_projetos: number,
}
export interface IContext extends IUser {
  authenticate: (user: string, password: string) => Promise<void>;
  logout: () => void;
}
export interface IAuthProvider{
  children: JSX.Element;
}
