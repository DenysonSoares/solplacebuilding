import router from "next/router";
import api from "../../services/api"
import { IUser } from "./types";

export function setUserLocalStorage(user: IUser | null){
  localStorage.setItem('User',JSON.stringify(user));
}

export function getUserLocalStorage() {
  const json = localStorage.getItem('User');
  
  if (!json) {
    return null;
  }

  const user = JSON.parse(json)

  return user ?? null;
}

