import { ReactNode } from "react";

export interface NewPassword{
  self_id: number
  id:number
  old_password: string,
  new_password: string
}

export type NewPasswordInput = Omit<NewPassword, 'id' | 'createdAt'>;

export interface NewPasswordsProviderProps{
  children: ReactNode;
}

export interface NewPasswordsContextData {
  passwords: NewPassword[];
  createNewPassword: (paswords: NewPasswordInput) => Promise<void>;
}