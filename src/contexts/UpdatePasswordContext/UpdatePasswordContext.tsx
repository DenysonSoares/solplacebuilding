import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { NewPassword, NewPasswordInput, NewPasswordsContextData, NewPasswordsProviderProps } from './types';

export const NewPasswordsContext = createContext<NewPasswordsContextData>(
  {} as NewPasswordsContextData
);

export function NewPasswordsProvider({children}: NewPasswordsProviderProps) {
  const [passwords, setNewPasswords] = useState<NewPassword[]>([]);

  async function createNewPassword(passwordsInput: NewPasswordInput){
    const { data } = await api.post('/solplace/user/redef_pass', {
      params:{
        ...passwordsInput
      } 
    })
    const { password } = data;
    console.log(data);
    setNewPasswords([
      ...passwords,
      password,
    ])
  }

  return(
    <NewPasswordsContext.Provider value={{passwords, createNewPassword}}>
      {children}
    </NewPasswordsContext.Provider>
  );

}