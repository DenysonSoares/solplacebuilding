import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { BasicProject, BasicProjectInput, BasicProjectsContextData, BasicProjectsProviderProps } from './types';
import { useAlert } from 'react-alert'


export const BasicProjectsContext = createContext<BasicProjectsContextData>(
  {} as BasicProjectsContextData
);

export function BasicProjectsProvider({children}: BasicProjectsProviderProps) {
  const warning = useAlert()
  const [basicprojects, setBasicProjects] = useState<BasicProject[]>([]);

  async function createBasicProject(basicprojectsInput: BasicProjectInput){
    try {
      const { data } = await api.post('/solplace/project/basic_info', {
        params:{
          ...basicprojectsInput
        } 
      })
      if (data.result?.ok){
        const { basicproject } = data;
        setBasicProjects([
          ...basicprojects,
          basicproject,
        ])
        warning.success("Projeto criado com sucesso!");
      }else{
        warning.error(data.result.error);
      }
    } catch (error) {
      return error;
    }
  }

  return(
    <BasicProjectsContext.Provider value={{basicprojects, createBasicProject}}>
      {children}
    </BasicProjectsContext.Provider>
  );

}