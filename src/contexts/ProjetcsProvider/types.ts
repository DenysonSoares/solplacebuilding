import { ReactNode } from 'react';

export interface BasicProject{
  name: string,
  valor_previsto: number,
  potencia: number,
  cidade_instalacao: string,
  estado_instalacao: string,
  client_name : string,
  email : string,
  telefone: string,
  localizacao: string,
  integrador_id: number
}

export type BasicProjectInput = Omit<BasicProject, 'id' | 'createdAt'>;

export interface BasicProjectsProviderProps{
  children: ReactNode;
}

export interface BasicProjectsContextData {
  basicprojects: BasicProject[];
  createBasicProject: (basicprojects: BasicProjectInput) => Promise<void>;
}
