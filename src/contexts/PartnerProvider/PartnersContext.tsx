import { createContext, useEffect, useState, ReactNode } from 'react';
import  api  from "../../services/api"
import { Partner, PartnerInput, PartnersContextData, PartnersProviderProps } from './types';



export const PartnersContext = createContext<PartnersContextData>(
  {} as PartnersContextData
);

export function PartnersProvider({children}: PartnersProviderProps) {
  const [partners, setPartners] = useState<Partner[]>([]);

  async function createPartner(partnersInput: PartnerInput){
    const { data } = await api.post('/solplace/socio/create', {
      params:{
        ...partnersInput
      } 
    })
    const { partner } = data;
    console.log(data);
    setPartners([
      ...partners,
      partner,
    ])
  }

  return(
    <PartnersContext.Provider value={{partners, createPartner}}>
      {children}
    </PartnersContext.Provider>
  );

}