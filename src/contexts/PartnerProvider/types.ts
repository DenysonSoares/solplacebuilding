import { ReactNode } from "react";

export interface Partner{
  nome_socio: string;
  data_nasc_socio: string;
  relacao_empresa: string;
  cargo_socio: string;
  cpf_socio: string;
  rg_socio: string;
  data_emissao_socio: string;
  orgao_emissor_socio: string;
  estado_emissor: string;
  naturalidade_socio: string;
  cidade_socio: string;
  escolaridade_socio: string;
  mae_socio: string;
  estado_civil_socio: string;
  renda_mensal_socio: number;
  patrimonio_socio: number;
}

export type PartnerInput = Omit<Partner, 'id' | 'createdAt'>;

export interface PartnersProviderProps{
  children: ReactNode;
}

export interface PartnersContextData {
  partners: Partner[];
  createPartner: (partners: PartnerInput) => Promise<void>;
}