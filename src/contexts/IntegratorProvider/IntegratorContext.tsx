import { createContext, useEffect, useState, } from 'react';
import  api  from "../../services/api";
import { Integrator, IntegratorInput, IntegratorsContextData, IntegratorsProviderProps } from './types';

export const IntegratorsContext = createContext<IntegratorsContextData>(
  {} as IntegratorsContextData
);

export function IntegratorsProvider({children}: IntegratorsProviderProps) {
  const [integrators, setIntegrators] = useState<Integrator[]>([]);

  async function createIntegrator(integratorsInput: IntegratorInput){
    const {data} = await api.post('/solplace/integradora/create', {
      params:{
        ...integratorsInput
      } 
    })
    const { integrator } = data;

    setIntegrators([
      ...integrators,
      integrator,
    ])
    console.log(data);
  }

  return(
    <IntegratorsContext.Provider value={{integrators, createIntegrator}}>
      {children}
    </IntegratorsContext.Provider>
  );

}