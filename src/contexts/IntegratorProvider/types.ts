import { ReactNode } from 'react';

export interface Integrator {
  name: string,
  l10n_br_legal_name: string,
  l10n_br_cnpj_cpf: string,
  banco: string,
  agencia: string,
  conta: string,
  cpf_cnpj_conta: string,
  zip: number,
  street: string,
  l10n_br_number: number,
  street2: string,
  city_id: string,
  state_id: string,
  country_id: string,
  l10n_br_district: string,
  ano_fundacao: number,
  numero_instalacao: number,
  qty_potencia: number,
  maior_potencia: number,
  nome_tecnico: string,
  nome_resp_conta: string,
  email_tecnico: string,
  celular_tecnico: number,
  crea: string,
  tipo_tecnico: string,
}


export type IntegratorInput = Omit<Integrator, 'id' | 'createdAt'>;

export interface IntegratorsProviderProps{
  children: ReactNode;
}

export interface IntegratorsContextData {
  integrators: Integrator[];
  createIntegrator: (integrators: IntegratorInput) => Promise<void>;
}