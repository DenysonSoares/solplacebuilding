import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ContenInformations } from "../../../src/components/HeaderInformation/styles";
import { Search } from "../../../src/components/Search";
import { AskContent, SearchAsc } from "../../../src/stylepages/faq";
import { faCoffee } from '@fortawesome/fontawesome-free-solid'
import { useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import 'react-accessible-accordion/dist/fancy-example.css';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

export default function Faq() {
  const [opencollapse, setOpenCollapse] = useState(false)
  return (

    <AskContent>
      <ContenInformations>
        <h1><strong>FAQ - Perguntas frequentes</strong></h1>
      </ContenInformations>
      <SearchAsc>
        <h1>Como podemos ajudar?</h1>
        <Search nameplaceholder="Faça uma pesquisa" />
      </SearchAsc>
      <Tabs>
        <TabList>
          <Tab>
            <FontAwesomeIcon icon="user" />
            <h4>Como alterar seu Perfil</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="university" />
            <h4>Bancos e crédito</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="at" />
            <h4>Envio de emails</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="dollar-sign" />
            <h4>Consulta de crédito</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="bolt" />
            <h4>Projeto fotovoltaico</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="user" />
            <h4>Sobre a SolPlace</h4>
          </Tab>
          <Tab>
            <FontAwesomeIcon icon="desktop" />
            <h4>Utilização da plataforma</h4>
          </Tab>
        </TabList>
        <TabPanel>
          <Accordion className="AskContent">
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        What harsh truths do you prefer to ignore?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        Exercitation in fugiat est ut ad ea cupidatat ut in
                        cupidatat occaecat ut occaecat consequat est minim minim
                        esse tempor laborum consequat esse adipisicing eu
                        reprehenderit enim.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        Is free will real or just an illusion?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                        In ad velit in ex nostrud dolore cupidatat consectetur
                        ea in ut nostrud velit in irure cillum tempor laboris
                        sed adipisicing eu esse duis nulla non.
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
          </Accordion>
        </TabPanel>
        <TabPanel>
          <h2>Any content 2</h2>
        </TabPanel>
      </Tabs>
    </AskContent>
  );
}