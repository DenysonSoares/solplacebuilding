import { WizardFormContent, WizardContent, WizardNav, ButtonChecked, NavButtons, NavigationSteps, StepWizard } from "../../../src/stylepages/perfil"
import { ContenInformations, InformationDate} from "../../../src/components/HeaderInformation/styles";
import WizardNavigation from "../../../src/components/Wizard/Navigation/WizardNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useState, FormEvent } from "react";
import WizardForm from "../../../src/components/Wizard/Navigation/WizardForm";
import { GlobalStyle } from "../../../styles/globals";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";
import { IntegratorsContext } from "../../contexts/IntegratorProvider/IntegratorContext";
import  Router  from "next/router";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from "formik";

export default function Perfil() {
  const [formStep, setFormStep] = useState(0);
  const buttonClass = ["noActive", "noActive", "noActive", "noActive", "noActive", "noActive"];
  function NextStepForm(){
    setFormStep(cur => cur + 1);
  }
  function PrevStepForm(){
    setFormStep(cur => cur - 1);

  }
  if (formStep == 0){
    buttonClass[0] = "inWrite"
  }
  if (formStep == 1){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inWrite"
  }
  if (formStep == 2){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inWrite"
  }
  if (formStep == 3){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inWrite"
  }
  if (formStep == 4){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inWrite"
  }
  if (formStep == 5){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inChecked"
    buttonClass[5] = "inWrite"
  }
  if (formStep == 6){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inChecked"
    buttonClass[5] = "inChecked"
  }

  const user = useContext(AuthContext)
  const {createIntegrator} = useContext(IntegratorsContext);

  const newIntegratorValidate = yup.object().shape({
    name: yup.string().required(),
    l10n_br_legal_name: yup.string().required(),
    l10n_br_cnpj_cpf:yup.number().required(),
    banco: yup.string().required(),
    agencia: yup.string().required(),
    conta: yup.string().required(),
    cpf_cnpj_conta: yup.string().required(),
    zip:yup.number().required(),
    street: yup.string().required(),
    l10n_br_number:yup.number().required(),
    street2: yup.string().required(),
    city_id: yup.string().required(),
    state_id: yup.string().required(),
    l10n_br_district: yup.string().required(),
    country_id: yup.string().required(),
    ano_fundacao:yup.number().required(),
    numero_instalacao:yup.number().required(),
    qty_potencia:yup.number().required(),
    maior_potencia:yup.number().required(),
    nome_tecnico: yup.string().required(),
    nome_resp_conta: yup.string().required(),
    email_tecnico: yup.string().required(),
    celular_tecnico:yup.number().required(),
    crea: yup.string().required(),
    tipo_tecnico: yup.string().required()
  })

  return (
    <>
      <WizardContent>
        <WizardNav>
          <h1>Perfil da Empresa</h1>
          <p><strong>Status:</strong>Completo</p>
          <NavButtons>   
            <ButtonChecked className={buttonClass[0]}>1</ButtonChecked>
            <ButtonChecked className={buttonClass[1]}>2</ButtonChecked>
            <ButtonChecked className={buttonClass[2]}>3</ButtonChecked>
            <ButtonChecked className={buttonClass[3]}>4</ButtonChecked>
            <ButtonChecked className={buttonClass[4]}>5</ButtonChecked>
            <ButtonChecked className={buttonClass[5]}>6</ButtonChecked>
          </NavButtons>
        </WizardNav>
        <WizardFormContent>
          <Formik
            validationSchema={newIntegratorValidate}
            initialValues={{
              name: '',
              l10n_br_legal_name: '',
              l10n_br_cnpj_cpf: '',
              banco: '',
              agencia: '',
              conta: '',
              cpf_cnpj_conta: '',
              zip: 0,
              street: '',
              l10n_br_number: 0,
              street2: '',
              city_id: '',
              state_id: '',
              l10n_br_district: '',
              country_id: '',
              ano_fundacao: 0,
              numero_instalacao: 0,
              qty_potencia: 0,
              maior_potencia: 0,
              nome_tecnico: '',
              nome_resp_conta: '',
              email_tecnico: '',
              celular_tecnico: 0,
              crea: '',
              tipo_tecnico: '',
            }}
            onSubmit={async (values) => {
              await createIntegrator(values);
              console.log(values)

              //Router.push('/home');
            }}
          >
          {({ errors }) => (
            <Form>
              {formStep == 0 && (
                <>
                  <ContenInformations>
                    <h2>Dados Básicos</h2>
                    <p>Atenção: Os dados básicos da empresa (CNPJ, razão social, nome fantasia, ano de fundação)
                    não poderão ser editados.</p>
                    <p>Dica: tenha cuidado e atenção ao preencher e revise antes de prosseguir.</p>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Razão Social
                      <Field 
                        type="text" 
                        name="l10n_br_legal_name"
                        id="l10n_br_legal_name"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.l10n_br_legal_name && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Nome fantasia
                      <Field 
                        type="text" 
                        name="name"
                        id="name"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.name && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      CNPJ
                      <Field 
                        type="text" 
                        name="l10n_br_cnpj_cpf"
                        id="l10n_br_cnpj_cpf"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.l10n_br_cnpj_cpf && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Ano de fundação
                      <Field 
                        type="text" 
                        name="ano_fundacao"
                        id="ano_fundacao"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.ano_fundacao && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 1 && (
                <>
                  <ContenInformations>
                      <h2>Endereço da empresa</h2>
                      <p>Inclua o endereço da sede de sua empresa</p>
                  </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                      CEP
                      <Field 
                        type="text" 
                        name="zip"
                        id="zip"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.zip && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="treeForms">
                      <label>
                        Logradouro
                        <Field 
                          type="text" 
                          name="street"
                          id="street"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.street && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Número
                        <Field 
                          type="text" 
                          name="l10n_br_number"
                          id="l10n_br_number"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_number && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Selecione um estado*
                        <Field 
                          type="text" 
                          name="state_id"
                          id="state_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.state_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="twoForm">
                      <label>
                        Bairro
                        <Field 
                          type="text" 
                          name="l10n_br_district"
                          id="l10n_br_district"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_district && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Cidade *
                        <Field 
                          type="text" 
                          name="city_id"
                          id="city_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.city_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <label>
                      Complemento
                      <Field 
                        type="text" 
                        name="street2"
                        id="street2"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.street2 && (<span>Esse campo é obrigatório</span>)}
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 2 && (
                <>
                  <ContenInformations>
                      <h2>Dados bancários</h2>
                      <p>Informe uma conta corrente válida e ativa de sua empresa</p>
                      <p>Apenas números, se sua agência/conta possui o dígito X, substitua-o por 0.</p>
                    </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                        Banco
                        <Field 
                          type="text" 
                          name="banco"
                          id="banco"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.banco && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Agência
                        <Field 
                          type="text" 
                          name="agencia"
                          id="agencia"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.agencia && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        Número da conta corrente
                        <Field 
                          type="text" 
                          name="conta"
                          id="conta"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.conta && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Nome do destinatário
                        <Field 
                          type="text" 
                          name="nome_resp_conta"
                          id="nome_resp_conta"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.nome_resp_conta && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        CNPJ da conta corrente
                        <Field 
                          type="text" 
                          name="cpf_cnpj_conta"
                          id="cpf_cnpj_conta"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.cpf_cnpj_conta && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 3 && (
                <>
                  <ContenInformations>
                    <h2>Responsável técnico da empresa</h2>
                    <p>É necessário ao menos um responsável técnico da empresa. Podendo ser engenheiro(a) ou técnico(a).</p>
                    <p>Atenção: Os dados do responsável técnico não poderão ser editados.</p>
                    <p>Dica: tenha cuidado e atenção ao preencher e revise antes de prosseguir.</p>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Nome do responsável
                      <Field 
                        type="text" 
                        name="nome_tecnico"
                        id="nome_tecnico"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.nome_tecnico && (<span>Esse campo é obrigatório</span>)}
                    </label>
                    <div className="twoForms">
                      <label>
                        Celular do responsável
                        <Field 
                          type="text" 
                          name="celular_tecnico"
                          id="celular_tecnico"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.celular_tecnico && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                      <label>
                        E-mail do responsável
                        <Field 
                          type="text" 
                          name="email_tecnico"
                          id="email_tecnico"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.email_tecnico && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                    </div>
                    <div className="twoForms">
                      <label>
                        CREA do responsável
                        <Field 
                          type="text" 
                          name="crea"
                          id="crea"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.crea && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                      <label>
                        Tipo de responsável
                        <Field 
                          type="text" 
                          name="tipo_tecnico"
                          id="tipo_tecnico"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.tipo_tecnico && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 4 && (
                <>
                  <ContenInformations>
                    <h2>Dados técnicos da empresa</h2>
                    <p>Dados técnicos da empresa</p>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Numero das instalações realizadas
                      <Field 
                        type="text" 
                        name="numero_instalacao"
                        id="numero_instalacao"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.numero_instalacao && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="user" />
                    </label>
                      <label>
                      Quantidade de potência instalada (em kWp)
                      <Field 
                        type="text" 
                        name="qty_potencia"
                        id="qty_potencia"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.qty_potencia && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="user" />
                    </label>
                      <label>
                      Maior potência instalada (em kWp)
                      <Field 
                        type="text" 
                        name="maior_potencia"
                        id="maior_potencia"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.maior_potencia && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="user" />
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 5 && (
                <>
                  <h1>Parabéns! Você concluiu o perfil da sua empresa</h1>
                  <NavigationSteps>
                    <button className="nextStep" type="submit">Criar perfil</button>
                  </NavigationSteps>
                </>
              )}
              
            </Form>
            )}
          </Formik>
          <NavigationSteps>
            {formStep > 0 && (
              <button className="prevStep" onClick={PrevStepForm}>Voltar</button>
            )}
            {formStep < 5 && (
              <button className="nextStep" onClick={NextStepForm}>Continuar</button>
            )}
          </NavigationSteps>
        </WizardFormContent>
      </WizardContent>
    </>
  );
}