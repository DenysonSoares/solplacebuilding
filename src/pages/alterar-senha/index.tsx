import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OrangeButton } from "../../components/OrangeButton";
import { BoxProject, ContentTabs } from "../../stylepages/projetos";
import { GlobalStyle } from "../../../styles/globals";
import { FormEvent, useContext, useState } from "react";

import  Router  from "next/router";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { ContentModal } from "../../stylepages/usuarios";
import { NewPasswordsContext } from "../../contexts/UpdatePasswordContext/UpdatePasswordContext";

export default function EditarSenha() {
  const user = useContext(AuthContext);
  const {createNewPassword} = useContext(NewPasswordsContext) 

  const newProjectValidate = yup.object().shape({
    old_password: yup.string().required(),
    new_password: yup.string().required(),
  })

  return (
    <>
      <ContentTabs>
        <BoxProject>
          <ContentModal>
            <h1>Alterar senha</h1>
            <p>Para alterar sua senha preencha todos os campos abaixo</p>
          </ContentModal>
          <Formik
            validationSchema={newProjectValidate}
            initialValues={{
              self_id: user.id,
              id: user.id,
              old_password: '',
              new_password: ''
            }}
            onSubmit={async (values) => {
              await createNewPassword(values);

              Router.push('/home');
            }}
          >  
          {({ errors }) => (
          <Form>
            <label>
              Senha atual *
              <Field 
                type="text"
                name="old_password"
                id="old_password"
                placeholder="Ex: Sol place"
              />
              {errors.old_password && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="bolt" />
            </label>
            <label>
              Nova senha *
              <Field 
                type="text" 
                name="new_password"
                id="new_password"
                placeholder="Ex: 2000"
              />
              {errors.new_password && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="dollar-sign" />
            </label>
            {/* <label>
              Confirmar nova senha *
              <Field 
                type="text" 
                name="confirmar_senha"
                id="confirmar_senha"
                placeholder="130"
              />
              {errors.confirmar_senha && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="bolt" />
            </label> */}
            <OrangeButton type="submit" buttontitle="Alterar senha" iconname="plus"/>
            </Form>
            )}
          </Formik>
        </BoxProject>
      </ContentTabs>
    </>
  );
}
