import { ContenInformations } from "../../../src/components/HeaderInformation/styles";
import { Search } from "../../../src/components/Search";
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas);
import { TableProjects } from "../../../src/components/TableProjects";
import { OrangeButton } from "../../../src/components/OrangeButton";
import { ContentTopUsuários } from "../../../src/stylepages/projetos";
import Link from "next/link";


export default function Projetos() {
  return (
    <>
      <ContenInformations>
        <h1><strong>Projetos</strong></h1>
        <p>Gerencie e tenha acesso a todos os usuários cadastrados de sua empresa</p>
      </ContenInformations>
      <ContentTopUsuários>
        <Link href="/projetos/criar-projeto"><a><OrangeButton buttontitle="Criar projeto" iconname="plus"/></a></Link>
        <Search nameplaceholder="Buscar projeto..."/>
      </ContentTopUsuários>
      <TableProjects />
    </>
  );
}