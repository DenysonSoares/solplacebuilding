import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ContenInformations } from "../../../components/HeaderInformation/styles";
import { OrangeButton } from "../../../components/OrangeButton";
import { BoxDocuments, BoxProject, TitleProject } from "../../../stylepages/projetos";
import Link from "next/link"
import { CardOverview, DashboardOverviewCards } from "../../../stylepages/home";

export default function PerfilDeCompra(){
  const router = useRouter();
  const ProjectId = router.query;
  console.log(ProjectId);

  return(
    <>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Perfil de compra</h2>
        <p><strong>Para prosseguir com o processo de compra do sistema fotovoltaico você deve fornecer algumas informações a respeito do cliente ou de sua empresa</strong></p>
        <p>As informações são de extrema importância para que o cliente obtenha as melhores ofertas de crédito</p>
        <p>As informações são de extrema importância para que o cliente obtenha as melhores ofertas de crédito</p>
        <p>As informações são de extrema importância para que o cliente obtenha as melhores ofertas de crédito</p>
      </ContenInformations>
      <DashboardOverviewCards>
        <CardOverview>
          <h4>Pessoa Física</h4>
          <article>
            <h1><FontAwesomeIcon icon="user" /></h1>
            <Link href={`/projetos/${ProjectId.projeto}/pessoa-fisica/`}><a className="orange-button">Pessoa Física <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
        <CardOverview>
          <h4>Pessoa Jurídica</h4>
          <article>
            <h1><FontAwesomeIcon icon="building" /></h1>
            <Link href={`/projetos/${ProjectId.projeto}/pessoa-juridica/`}><a className="orange-button">Pessoa Jurídica <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
      </DashboardOverviewCards>
    </>
  );
}