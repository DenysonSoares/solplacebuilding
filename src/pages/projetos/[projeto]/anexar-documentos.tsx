import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
	import { useRouter } from "next/router"
import React, { useState } from "react";
import { ContenInformations } from "../../../components/HeaderInformation/styles";
import { OrangeButton } from "../../../components/OrangeButton";
import { BoxDocuments, BoxProject, TitleProject } from "../../../stylepages/projetos";

export default function AnexarDocumentos(){
  const router = useRouter();

  const [rg, setRg] = useState("Arquivo pendente");
  const [cpf, setCpf] = useState("Arquivo pendente");
  const [pendencia, setPendencia] = useState("Arquivo pendente");
  const [contaEnergia, setContaEnergia] = useState("Arquivo pendente");
  const [impostaRenda, setImpostoRenda] = useState("Arquivo pendente");
  const [reciboImposto, setReciboImposto] = useState("Arquivo pendente");
  const [certidaoCasamento, setCertidaoCasamento] = useState("Arquivo pendente");
  const [rgConjuge, setRgConjuge] = useState("Arquivo pendente");
  const [cpfconjuge, setCpfConjuge] = useState("Arquivo pendente");

  return(
    
    <>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Anexar Documentos</h2>
        <p>Gerencie e tenha acesso a todos os usuários cadastrados de sua empresa</p>
      </ContenInformations>
      <BoxDocuments>
        <h5>Documentação necessária</h5>
        <p><strong>Dica:</strong> Você pode enviar os documentos parcialmente. Não é necessário enviar todos de uma só vez.</p>
        <form action="">
          <section>
            <p>RG</p>
            <p>{rg}</p>
            <input onChange={(event) => setRg(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>CPF</p>
            <p>{cpf}</p>
            <input onChange={(event) => setCpf(event.target.files[0].name)}  type="file" required/>
          </section>
          <section>
            <p>Pendência</p>
            <p>{pendencia}</p>
            <input onChange={(event) => setPendencia(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>Última conta de energia</p>
            <p>{contaEnergia}</p>
            <input onChange={(event) => setContaEnergia(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>Declaração de Imposto de Renda (2019/2020)</p>
            <p>{impostaRenda}</p>
            <input onChange={(event) => setImpostoRenda(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>Recibo do Imposto de Renda (2019/2020)</p>
            <p>{reciboImposto}</p>
            <input onChange={(event) => setReciboImposto(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>Certidão de casamento</p>
            <p>{certidaoCasamento}</p>
            <input onChange={(event) => setCertidaoCasamento(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>RG do Cônjuge</p>
            <p>{rgConjuge}</p>
            <input onChange={(event) => setRgConjuge(event.target.files[0].name)} type="file" required/>
          </section>
          <section>
            <p>CPF do Cônjuge</p>
            <p>{cpfconjuge}</p>
            <input onChange={(event) => setCpfConjuge(event.target.files[0].name)} type="file" required/>
          </section>
          <OrangeButton buttontitle="Enviar documentos" iconname="plus"/>
        </form>
      </BoxDocuments>
    </>
  );
}