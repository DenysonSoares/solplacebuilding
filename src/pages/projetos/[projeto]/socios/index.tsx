import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { ContenInformations } from "../../../../components/HeaderInformation/styles";
import { OrangeButton } from "../../../../components/OrangeButton";
import { DashboardSocios, ListSocios, CardSocio } from "../../../../stylepages/projetos";
import Link from "next/link"

export default function Socios(){
  const router = useRouter();
    const ProjectId = router.query;

  return(
    <div>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Cadastrar sócios</h2>
      </ContenInformations>
      <DashboardSocios>
        <Link href={`/projetos/${ProjectId.projeto}/socios/criar-socio/`}>
          <a><OrangeButton buttontitle="Cadastrar sócio" iconname="plus"/></a>
        </Link>
        <ListSocios>
          <CardSocio>
            <header>
              <h4>Nome do sócio</h4>
              <button>...</button>
            </header>
            <article>
              <p><strong>CPF:</strong> </p>
              <p><strong>Data de nascimento:</strong> </p>
              <p><strong>Telefone/celular:</strong> </p>
              <p><strong>E-mail:</strong> </p>
              <p><strong>Relação com a empresa:</strong> </p>
              <p><strong>Cargo:</strong> </p>
            </article>
          </CardSocio>
          <CardSocio>
            <header>
              <h4>Nome do sócio</h4>
              <button>...</button>
            </header>
            <article>
              <p><strong>CPF:</strong> </p>
              <p><strong>Data de nascimento:</strong> </p>
              <p><strong>Telefone/celular:</strong> </p>
              <p><strong>E-mail:</strong> </p>
              <p><strong>Relação com a empresa:</strong> </p>
              <p><strong>Cargo:</strong> </p>
            </article>
          </CardSocio>
          <CardSocio>
            <header>
              <h4>Nome do sócio</h4>
              <button>...</button>
            </header>
            <article>
              <p><strong>CPF:</strong> </p>
              <p><strong>Data de nascimento:</strong> </p>
              <p><strong>Telefone/celular:</strong> </p>
              <p><strong>E-mail:</strong> </p>
              <p><strong>Relação com a empresa:</strong> </p>
              <p><strong>Cargo:</strong> </p>
            </article>
          </CardSocio>
        </ListSocios>
      </DashboardSocios>
    </div>
  );
}