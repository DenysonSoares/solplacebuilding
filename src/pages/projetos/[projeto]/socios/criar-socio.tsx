import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OrangeButton } from "../../../../../src/components/OrangeButton";
import { BoxProject, ContentTabs } from "../../../../../src/stylepages/projetos";
import { GlobalStyle } from "../../../../../styles/globals";
import { FormEvent, useContext, useState } from "react";
import { BasicProjectsContext } from "../../../../contexts/ProjetcsProvider/ProjectContext";
import  Router  from "next/router";
import { AuthContext } from "../../../../contexts/AuthProvider/AuthContext";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { PartnersContext } from "../../../../contexts/PartnerProvider/PartnersContext";
import { useRouter } from "next/router";

export default function CriarProjeto() {
  const user = useContext(AuthContext);
  const router = useRouter();
  const ProjectId = router.query;
  const {createPartner} = useContext(PartnersContext); 

  const newPartnerValidate = yup.object().shape({
    // name: yup.string().required(),
    // valor_previsto: yup.number().required(),
    // potencia: yup.number().required(),
    // cidade_instalacao: yup.string().required(),
    // estado_instalacao: yup.string().required(),
    // client_name: yup.string().required(),
    // email: yup.string().email().required(),
    // telefone: yup.string().min(11).required(),
  })

  return (
    <>
      <ContentTabs>
        <BoxProject>
          <h2>Adicionar novo sócio</h2>
        </BoxProject>
        <BoxProject>
          <Formik
            validationSchema={newPartnerValidate}
            initialValues={{
              nome_socio: 'Carlinhos',
              data_nasc_socio: '2021-10-15',
              relacao_empresa: 'Fundador',
              cargo_socio: 'Gerente',
              cpf_socio: '12356345784389',
              rg_socio: '342456345',
              data_emissao_socio: '2018-08003',
              orgao_emissor_socio: 'SSP',
              estado_emissor: 'São Paulo',
              naturalidade_socio: 'São Paulo',
              cidade_socio: 'São Paulo',
              escolaridade_socio: 'Graduação',
              mae_socio: 'Maria Cecilia',
              estado_civil_socio: 'casado',
              renda_mensal_socio: 12000,
              patrimonio_socio: 500000,
              projeto_id: ProjectId.projeto
            }}
            onSubmit={async (values) => {
              await createPartner(values);
              console.log(values);
              //Router.push('/projetos');
            }}
          >  
          {({ errors }) => (
          <Form>
            <label>
              Nome do projeto *
              <Field 
                type="text"
                name="nome_socio"
                id="nome_socio"
                placeholder="Ex: Sol place"
              />
              {errors.nome_socio && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="bolt" />
            </label>
            <div className="treeForms">
              <label>
                Data de nascimento
                <Field 
                  type="text"
                  name="data_nasc_socio"
                  id="data_nasc_socio"
                  placeholder="Ex: Sol place"
                />
                {errors.data_nasc_socio && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="bolt" />
              </label>
              <label>
                Relação com a empresa
                <Field 
                  type="text"
                  name="relacao_empresa"
                  id="relacao_empresa"
                  placeholder="Ex: Sol place"
                />
                {errors.relacao_empresa && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="bolt" />
              </label>
               <label>
                Cargo
                <Field 
                  type="text"
                  name="cargo_socio"
                  id="cargo_socio"
                  placeholder="Ex: Sol place"
                />
                {errors.cargo_socio && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="bolt" />
              </label>
            </div>
            <div className="twoForms"> 
              <label>
                CPF
                <Field 
                  type="text"
                  name="cpf_socio"
                  id="cpf_socio"
                  placeholder="Ex: Sol place"
                />
                {errors.cpf_socio && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="bolt" />
              </label>
              <label>
                RG
                <Field 
                  type="text"
                  name="rg_socio"
                  id="rg_socio"
                  placeholder="Ex: Sol place"
                />
                {errors.rg_socio && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="bolt" />
              </label>
            </div>
            <div className="treeForms">
                <label>
                  Data de emissão
                  <Field 
                    type="text"
                    name="data_emissao_socio"
                    id="data_emissao_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.data_emissao_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Orgão emissor
                  <Field 
                    type="text"
                    name="orgao_emissor_socio"
                    id="orgao_emissor_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.orgao_emissor_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Orgão emissor
                  <Field 
                    type="text"
                    name="orgao_emissor_socio"
                    id="orgao_emissor_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.orgao_emissor_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
              </div>
              <div className="treeForms">
                <label>
                  Estado emissor
                  <Field 
                    type="text"
                    name="estado_emissor"
                    id="estado_emissor"
                    placeholder="Ex: Sol place"
                  />
                  {errors.estado_emissor && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Naturalidade
                  <Field 
                    type="text"
                    name="naturalidade_socio"
                    id="naturalidade_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.naturalidade_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Cidade 
                  <Field 
                    type="text"
                    name="cidade_socio"
                    id="cidade_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.cidade_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
              </div>
              <div className="treeForms">
                <label>
                  Escolaridade 
                  <Field 
                    type="text"
                    name="escolaridade_socio"
                    id="escolaridade_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.escolaridade_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Mãe 
                  <Field 
                    type="text"
                    name="mae_socio"
                    id="mae_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.mae_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Estado civil 
                  <Field 
                    type="text"
                    name="estado_civil_socio"
                    id="estado_civil_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.estado_civil_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
              </div>
              <div>
                <label>
                  Renda mensal
                  <Field 
                    type="text"
                    name="renda_mensal_socio"
                    id="renda_mensal_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.renda_mensal_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
                <label>
                  Patrimônio
                  <Field 
                    type="text"
                    name="patrimonio_socio"
                    id="patrimonio_socio"
                    placeholder="Ex: Sol place"
                  />
                  {errors.patrimonio_socio && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="bolt" />
                </label>
              </div>
            <OrangeButton type="submit" buttontitle="Criar projeto" iconname="plus"/>
            </Form>
            )}
          </Formik>
        </BoxProject>
      </ContentTabs>
    </>
  );
}
