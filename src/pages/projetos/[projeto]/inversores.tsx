import { ContenInformations } from "../../../components/HeaderInformation/styles";
import { Search } from "../../../components/Search";
import { ContentTopUsuários, ContentModal, ActionsButtonModal } from "../../../stylepages/usuarios";
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { TableUsers } from "../../../components/TableUsers";
import { OrangeButton } from "../../../components/OrangeButton";
import Modal from 'react-modal';
import { FormEvent, useContext, useState } from "react";
import api from "../../../services/api";
import { UsersContext, UsersProvider } from "../../../contexts/UserProvider/UsersContext";
import  Router  from "next/router";
import { AuthContext } from "../../../contexts/AuthProvider/AuthContext";
library.add(fas);
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { useRouter } from "next/router";

export default function Inversores(props) {
  const router = useRouter();
  const ProjectId = router.query;
  console.log(ProjectId);

  const user = useContext(AuthContext)
  const [isNewUserModalOpen, setIsNewUserModalOpen] = useState(false);
  const {createUser} = useContext(UsersContext);

  const newUserValidate = yup.object().shape({
    name: yup.string().required(),
    telefone: yup.string().min(11).required(),
    email: yup.string().email().required(),
    cargo: yup.string().required(),
    password: yup.string().required()
  })

  function handleOpenNewUserModal(){
    setIsNewUserModalOpen(true);
  }

  function handleCloseNewUserModal(){
    setIsNewUserModalOpen(false);
  }
  return (
    <>
      <Modal 
        isOpen={isNewUserModalOpen} 
        onRequestClose={handleCloseNewUserModal} 
        overlayClassName="react-modal-overlay"
        className="react-modal-content"
      >
        <ContentModal>
          <div>
            <h1>Criar inversor</h1>
            <Formik
              validationSchema={newUserValidate}
              initialValues={{
                name: '',
                telefone: 0,
                email: '',
                cargo: '',
                password: '',
                admin_id: user.admin_id
              }}
              onSubmit={async (values) => {
                await createUser(values);

                Router.push('/usuarios');
              }}
            >
            {({ errors }) => (
              <Form>
                <label>
                  Nome do usuário *
                  <Field 
                    type="text" 
                    name="name"
                    id="name"
                    placeholder="Ex: Antônio Mendes"
                  />
                  {errors.name && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="user" />
                </label>
                <label>
                  Celular do usuário *
                  <Field  
                    type="text" 
                    name="telefone"
                    id="telefone"
                    placeholder="(00)00000-00000"
                  />
                  {errors.telefone && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="phone" />
                </label>
                <label>
                  E-mail do usuário *
                  <Field 
                    type="text" 
                    name="email"
                    id="email"
                    placeholder="Ex: antonio@email.com"
                  />
                  {errors.email && (<span>Email inválido</span>)}
                  <FontAwesomeIcon icon="envelope" />
                </label>
                <label>
                  Cargo do usuário *
                  <Field
                    component="select"
                    id="cargo"
                    name="cargo"
                  >
                    <option value="admin">Administrador</option>
                    <option value="vendedor">Vendedor</option>
                  </Field>
                  {errors.cargo && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="user" />
                </label>
                <label>
                  Senha do usuário *
                  <Field 
                    type="text" 
                    name="password"
                    id="password"
                  />
                  {errors.password && (<span>Esse campo é obrigatório</span>)}
                  <FontAwesomeIcon icon="user" />
                </label>
                <ActionsButtonModal>
                  <button className="backButton" onClick={handleCloseNewUserModal}>Voltar</button>
                  <OrangeButton type="submit" buttontitle="Criar usuário" iconname="plus"/>
                </ActionsButtonModal>
              </Form>
              )}
            </Formik>
          </div>
        </ContentModal>
      </Modal>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Inversores</h2>
      </ContenInformations>
      <ContentTopUsuários>
        <OrangeButton buttontitle="Criar inversor" iconname="plus" handleClick={handleOpenNewUserModal}/>
        <Search nameplaceholder="Buscar usuário..."/>
      </ContentTopUsuários>
      <TableUsers />
    </>
  );
}