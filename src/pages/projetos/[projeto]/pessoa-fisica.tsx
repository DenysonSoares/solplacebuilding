import { WizardFormContent, WizardContent, WizardNav, ButtonChecked, NavButtons, NavigationSteps, StepWizard } from "../../../../src/stylepages/perfil"
import { ContenInformations, InformationDate} from "../../../../src/components/HeaderInformation/styles";
import WizardNavigation from "../../../../src/components/Wizard/Navigation/WizardNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useState } from "react";
import WizardForm from "../../../../src/components/Wizard/Navigation/WizardForm";
import { GlobalStyle } from "../../../../styles/globals";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { FisicPersonContext } from "../../../contexts/FisicPersonProvider/FisicPersonContext";
import { AuthContext } from "../../../contexts/AuthProvider/AuthContext";
import { useRouter } from "next/router";

export default function PessoaFísica() {
  const [formStep, setFormStep] = useState(0);
  const buttonClass = ["noActive", "noActive", "noActive", "noActive", "noActive", "noActive"];
  function NextStepForm(){
    setFormStep(cur => cur + 1);
  }
  function PrevStepForm(){
    setFormStep(cur => cur - 1);

  }
  if (formStep == 0){
    buttonClass[0] = "inWrite"
  }
  if (formStep == 1){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inWrite"
  }
  if (formStep == 2){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inWrite"
  }
  if (formStep == 3){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inWrite"
  }
  if (formStep == 4){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inWrite"
  }
  if (formStep == 5){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inChecked"
    buttonClass[5] = "inChecked"
  }

  const router = useRouter();
  const ProjectId = router.query;
  console.log(ProjectId.projeto);
  const user = useContext(AuthContext)
  const {createFisicPerson} = useContext(FisicPersonContext);
  const newFisicPersinValidate = yup.object().shape({
    name: yup.string().required(), 
    data_nascimento: yup.string().required(), 
    email: yup.string().required(),
    phone: yup.number().required(),
    zip: yup.string().required(), 
    street: yup.string().required(), 
    l10n_br_number: yup.number().required(),                                
    l10n_br_district: yup.string().required(), 
    street2: yup.string().required(),
    state_id: yup.string().required(),
    city_id: yup.string().required(), 
    l10n_br_cnpj_cpf: yup.string().required(),                      
    nacionalidade: yup.string().required(),
    genero: yup.string().required(),
    naturalidade: yup.string().required(),
    escolaridade: yup.string().required(),
    nome_mae: yup.string().required(),
    estado_civil: yup.string().required(),
    nome_conjuge: yup.string().required(),
    cpf_conjuge: yup.string().required(),
    data_nasc_conjuge: yup.string().required(),
    orgao_emissor: yup.string().required(),
    rg: yup.string().required(),
    data_emissao: yup.string().required(), 
    uf_emissor: yup.string().required(),
    ocupacao: yup.string().required(),
    cargo: yup.string().required(),
    profissao: yup.string().required(),
    renda_mensal: yup.number().required(),
    patrimonio: yup.number().required(),
    company_type: yup.string().required(),
  })

  return (
    <>
      <WizardContent>
        <WizardNav>
          <h1>Pessoa Física</h1>
          <p><strong>Status:</strong>Completo</p>
          <NavButtons>   
            <ButtonChecked className={buttonClass[0]}>1</ButtonChecked>
            <ButtonChecked className={buttonClass[1]}>2</ButtonChecked>
            <ButtonChecked className={buttonClass[2]}>3</ButtonChecked>
            <ButtonChecked className={buttonClass[3]}>4</ButtonChecked>
            <ButtonChecked className={buttonClass[4]}>5</ButtonChecked>
          </NavButtons>
        </WizardNav>
        <WizardFormContent>
          <Formik
            validationSchema={newFisicPersinValidate}
            initialValues={{
              name: '', 
              data_nascimento: '', 
              email: '',
              phone: 123425767123,
              zip: '', 
              street: '', 
              l10n_br_number: 10,                                
              l10n_br_district: '', 
              street2: '',
              state_id: '',
              city_id: '', 
              l10n_br_cnpj_cpf: '',                      
              nacionalidade: '',
              genero: '',
              naturalidade: '',
              escolaridade: '',
              nome_mae: '',
              estado_civil: '',
              nome_conjuge: '',
              cpf_conjuge: '',
              data_nasc_conjuge: '',
              orgao_emissor: '',
              rg: '', 
              data_emissao: '',
              uf_emissor: '',
              ocupacao: '',
              cargo: '',
              profissao: '',
              renda_mensal: 7000,
              patrimonio: 100000,
              company_type: '',
              projeto_id: ProjectId.projeto,
            }}
            onSubmit={async (values) => {
              await createFisicPerson(values);
            }}
          >
          {({ errors }) => (
            <Form>
              {formStep == 0 && (
                <>
                  <ContenInformations>
                    <h2>Dados Básicos</h2>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Name
                      <Field 
                        type="text" 
                        name="name"
                        id="name"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.name && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Data de nascimento
                      <Field 
                        type="text" 
                        name="data_nascimento"
                        id="data_nascimento"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.data_nascimento && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Email
                      <Field 
                        type="text" 
                        name="email"
                        id="email"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.email && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Telefone
                      <Field 
                        type="text" 
                        name="phone"
                        id="phone"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.phone && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 1 && (
                <>
                  <ContenInformations>
                      <h2>Endereço da empresa</h2>
                      <p>Inclua o endereço da sede de sua empresa</p>
                  </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                      CEP
                      <Field 
                        type="text" 
                        name="zip"
                        id="zip"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.zip && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="treeForms">
                      <label>
                        Logradouro
                        <Field 
                          type="text" 
                          name="street"
                          id="street"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.street && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Número
                        <Field 
                          type="text" 
                          name="l10n_br_number"
                          id="l10n_br_number"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_number && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Selecione um estado*
                        <Field 
                          type="text" 
                          name="state_id"
                          id="state_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.state_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="twoForm">
                      <label>
                        Bairro
                        <Field 
                          type="text" 
                          name="l10n_br_district"
                          id="l10n_br_district"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_district && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Cidade *
                        <Field 
                          type="text" 
                          name="city_id"
                          id="city_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.city_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <label>
                      Complemento
                      <Field 
                        type="text" 
                        name="street2"
                        id="street2"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.street2 && (<span>Esse campo é obrigatório</span>)}
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 2 && (
                <>
                  <ContenInformations>
                      <h2>Dados pessoais</h2>
                    </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                        CPF
                        <Field 
                          type="text" 
                          name="l10n_br_cnpj_cpf"
                          id="l10n_br_cnpj_cpf"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_cnpj_cpf && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        RG
                        <Field 
                          type="text" 
                          name="rg"
                          id="rg"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.rg && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Data de emissão
                        <Field 
                          type="text" 
                          name="data_emissao"
                          id="data_emissao"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.data_emissao && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Orgão Emissor
                        <Field 
                          type="text" 
                          name="orgao_emissor"
                          id="orgao_emissor"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.orgao_emissor && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        UF Emissor
                        <Field 
                          type="text" 
                          name="uf_emissor"
                          id="uf_emissor"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.uf_emissor && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Nacionalidade
                        <Field 
                          type="text" 
                          name="nacionalidade"
                          id="nacionalidade"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.nacionalidade && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        Genero
                        <Field 
                          type="text" 
                          name="genero"
                          id="genero"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.genero && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Naturalidade
                        <Field 
                          type="text" 
                          name="naturalidade"
                          id="naturalidade"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.naturalidade && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        Escolaridade
                        <Field 
                          type="text" 
                          name="escolaridade"
                          id="escolaridade"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.escolaridade && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Nome da mãe
                        <Field 
                          type="text" 
                          name="nome_mae"
                          id="nome_mae"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.nome_mae && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div>
                      <div className="twoForms">
                        <label>
                          Estado cívil
                          <Field 
                            type="text" 
                            name="estado_civil"
                            id="estado_civil"
                            placeholder="Ex: Antônio Mendes"
                          />
                          {errors.estado_civil && (<span>Esse campo é obrigatório</span>)}
                        </label>
                        <label>
                          Nome do Conjuge
                          <Field 
                            type="text" 
                            name="nome_conjuge"
                            id="nome_conjuge"
                            placeholder="Ex: Antônio Mendes"
                          />
                          {errors.nome_conjuge && (<span>Esse campo é obrigatório</span>)}
                        </label>
                        <label>
                          CPF do conjuge
                          <Field 
                            type="text" 
                            name="cpf_conjuge"
                            id="cpf_conjuge"
                            placeholder="Ex: Antônio Mendes"
                          />
                          {errors.cpf_conjuge && (<span>Esse campo é obrigatório</span>)}
                        </label>
                        <label>
                          Data de nascimento do conjuge
                          <Field 
                            type="text" 
                            name="data_nasc_conjuge"
                            id="data_nasc_conjuge"
                            placeholder="Ex: Antônio Mendes"
                          />
                          {errors.data_nasc_conjuge && (<span>Esse campo é obrigatório</span>)}
                        </label>
                      </div>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 3 && (
                <>
                  <ContenInformations>
                    <h2>Dados profissionais</h2>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Ocupação
                      <Field 
                        type="text" 
                        name="ocupacao"
                        id="ocupacao"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.ocupacao && (<span>Esse campo é obrigatório</span>)}
                    </label>
                    <div className="twoForms">
                      <label>
                        Cargo
                        <Field 
                          type="text" 
                          name="cargo"
                          id="cargo"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.cargo && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                      <label>
                        Profissão
                        <Field 
                          type="text" 
                          name="profissao"
                          id="profissao"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.profissao && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                    </div>
                    <div className="twoForms">
                      <label>
                        Renda Mensal
                        <Field 
                          type="text" 
                          name="renda_mensal"
                          id="renda_mensal"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.renda_mensal && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                      <label>
                        Patrimônio
                        <Field 
                          type="text" 
                          name="patrimonio"
                          id="patrimonio"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.patrimonio && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 4 && (
                <>
                  <h1>Parabéns! Você concluiu o perfil da sua empresa</h1>
                  <NavigationSteps>
                    <button className="nextStep" type="submit">Criar perfil</button>
                  </NavigationSteps>
                </>
              )}
              
            </Form>
            )}
          </Formik>
          <NavigationSteps>
            {formStep > 0 && (
              <button className="prevStep" onClick={PrevStepForm}>Voltar</button>
            )}
            {formStep < 4 && (
              <button className="nextStep" onClick={NextStepForm}>Continuar</button>
            )}
          </NavigationSteps>
        </WizardFormContent>
      </WizardContent>
    </>
  );
}