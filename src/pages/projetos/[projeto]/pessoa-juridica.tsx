import { WizardFormContent, WizardContent, WizardNav, ButtonChecked, NavButtons, NavigationSteps, StepWizard } from "../../../stylepages/perfil"
import { ContenInformations, InformationDate} from "../../../components/HeaderInformation/styles";
import WizardNavigation from "../../../components/Wizard/Navigation/WizardNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useState } from "react";
import WizardForm from "../../../components/Wizard/Navigation/WizardForm";
import { GlobalStyle } from "../../../../styles/globals";
import { JuridicPersonContext } from "../../../contexts/JuridicPersonProvider/JuridicPersonContext";
import { useRouter } from "next/router";
import { AuthContext } from "../../../contexts/AuthProvider/AuthContext";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';

export default function PessoaJuridica() {
  const [formStep, setFormStep] = useState(0);
  const buttonClass = ["noActive", "noActive", "noActive", "noActive", "noActive", "noActive"];
  function NextStepForm(){
    setFormStep(cur => cur + 1);
  }
  function PrevStepForm(){
    setFormStep(cur => cur - 1);

  }
  if (formStep == 0){
    buttonClass[0] = "inWrite"
  }
  if (formStep == 1){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inWrite"
  }
  if (formStep == 2){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inWrite"
  }
  if (formStep == 3){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inWrite"
  }
  if (formStep == 4){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inWrite"
  }
  if (formStep == 5){
    buttonClass[0] = "inChecked"
    buttonClass[1] = "inChecked"
    buttonClass[2] = "inChecked"
    buttonClass[3] = "inChecked"
    buttonClass[4] = "inChecked"
    buttonClass[5] = "inChecked"
  }

  const router = useRouter();
  const ProjectId = router.query;
  console.log(ProjectId.projeto);
  const user = useContext(AuthContext)
  const {createJuridicPerson} = useContext(JuridicPersonContext);
  
  const newFisicPersinValidate = yup.object().shape({
    name: yup.string().required(),
    email: yup.string().required(),
    phone: yup.number().required(),
    zip: yup.string().required(),
    street: yup.string().required(),
    l10n_br_number: yup.number().required(),
    l10n_br_district: yup.string().required(),
    street2: yup.string().required(),
    state_id: yup.string().required(),
    city_id: yup.string().required(),
    l10n_br_cnpj_cpf: yup.string().required(),
    company_type: yup.string().required(),
    l10n_br_legal_name: yup.string().required(),
    data_fundacao: yup.string().required(),
    natureza_legal: yup.string().required(),
    tamanho_empresa: yup.string().required(),
    numero_func: yup.number().required(),
    alteracoes_contrato: yup.number().required(),
    website: yup.string().required(),
    l10n_br_inscr_est: yup.string().required(),
    l10n_br_inscr_mun: yup.string().required(),
    faturamento_anual: yup.number().required(),
    atividade_economica: yup.string().required(),
  })

  return (
    <>
      <WizardContent>
        <WizardNav>
          <h1>Pessoa Jurídica</h1>
          <p><strong>Status:</strong>Completo</p>
          <NavButtons>   
            <ButtonChecked className={buttonClass[0]}>1</ButtonChecked>
            <ButtonChecked className={buttonClass[1]}>2</ButtonChecked>
            <ButtonChecked className={buttonClass[2]}>3</ButtonChecked>
            <ButtonChecked className={buttonClass[3]}>4</ButtonChecked>
            <ButtonChecked className={buttonClass[4]}>5</ButtonChecked>
          </NavButtons>
        </WizardNav>
        <WizardFormContent>
          <Formik
            validationSchema={newFisicPersinValidate}
            initialValues={{
              name: 'Empresa legal',
              email: 'empresa@pj.com.br',
              phone: 12345644532,
              zip: '13241234',
              street: 'Rua das avenidas',
              l10n_br_number: 100,
              l10n_br_district: 'Ruazinha',
              street2: 'Ruazona',
              state_id: 'São Paulo',
              city_id: 'São Paulo',
              l10n_br_cnpj_cpf: '12525231623843',
              company_type: 'company',
              l10n_br_legal_name: 'Noma razão',
              data_fundacao: '2021-10-12',
              natureza_legal: 'natureza',
              tamanho_empresa: 'media',
              numero_func: 50,
              alteracoes_contrato: 5,
              website: 'www.teste.com.br',
              l10n_br_inscr_est: '523412345',
              l10n_br_inscr_mun: '432164243',
              faturamento_anual: 100,
              atividade_economica: 'asdftweaasdf',
              projeto_id: ProjectId.projeto
            }}
            onSubmit={async (values) => {
              await createJuridicPerson(values);
              console.log(values)

              //Router.push('/home');
            }}
          >
          {({ errors }) => (
            <Form>
              {formStep == 0 && (
                <>
                  <ContenInformations>
                    <h2>Dados Básicos</h2>
                  </ContenInformations>
                  <StepWizard>
                    <label>
                      Nome fantasia
                      <Field 
                        type="text" 
                        name="name"
                        id="name"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.name && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Razão social
                      <Field 
                        type="text" 
                        name="l10n_br_legal_name"
                        id="l10n_br_legal_name"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.name && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Email
                      <Field 
                        type="text" 
                        name="email"
                        id="email"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.email && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                    <label>
                      Telefone
                      <Field 
                        type="text" 
                        name="phone"
                        id="phone"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.phone && (<span>Esse campo é obrigatório</span>)}
                      <FontAwesomeIcon icon="lock" />
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 1 && (
                <>
                  <ContenInformations>
                      <h2>Endereço da empresa</h2>
                      <p>Inclua o endereço da sede de sua empresa</p>
                  </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                      CEP
                      <Field 
                        type="text" 
                        name="zip"
                        id="zip"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.zip && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="treeForms">
                      <label>
                        Logradouro
                        <Field 
                          type="text" 
                          name="street"
                          id="street"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.street && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Número
                        <Field 
                          type="text" 
                          name="l10n_br_number"
                          id="l10n_br_number"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_number && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Selecione um estado*
                        <Field 
                          type="text" 
                          name="state_id"
                          id="state_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.state_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <div className="twoForm">
                      <label>
                        Bairro
                        <Field 
                          type="text" 
                          name="l10n_br_district"
                          id="l10n_br_district"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_district && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Cidade *
                        <Field 
                          type="text" 
                          name="city_id"
                          id="city_id"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.city_id && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                    <label>
                      Complemento
                      <Field 
                        type="text" 
                        name="street2"
                        id="street2"
                        placeholder="Ex: Antônio Mendes"
                      />
                      {errors.street2 && (<span>Esse campo é obrigatório</span>)}
                    </label>
                  </StepWizard>
                </>
              )}
              {formStep == 2 && (
                <>
                  <ContenInformations>
                      <h2>Dados pessoais</h2>
                    </ContenInformations>
                  <StepWizard>
                    <div className="twoForms">
                      <label>
                        CNPJ
                        <Field 
                          type="text" 
                          name="l10n_br_cnpj_cpf"
                          id="l10n_br_cnpj_cpf"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_cnpj_cpf && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Tipo da empresa
                        <Field 
                          type="text" 
                          name="company_type"
                          id="company_type"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.company_type && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Natureza legal
                        <Field 
                          type="text" 
                          name="natureza_legal"
                          id="natureza_legal"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.natureza_legal && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Numero de funcionarios
                        <Field 
                          type="text" 
                          name="numero_func"
                          id="numero_func"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.numero_func && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Alterações de contrato
                        <Field 
                          type="text" 
                          name="alteracoes_contrato"
                          id="alteracoes_contrato"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.alteracoes_contrato && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Website
                        <Field 
                          type="text" 
                          name="website"
                          id="website"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.website && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        Data fundação
                        <Field 
                          type="text" 
                          name="data_fundacao"
                          id="data_fundacao"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.data_fundacao && (<span>Esse campo é obrigatório</span>)}
                      </label>
                      <label>
                        Inscrição estadual
                        <Field 
                          type="text" 
                          name="l10n_br_inscr_est"
                          id="l10n_br_inscr_est"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_inscr_est && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                      <div className="twoForms">
                      <label>
                        Escolaridade
                        <Field 
                          type="text" 
                          name="l10n_br_inscr_mun"
                          id="l10n_br_inscr_mun"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.l10n_br_inscr_mun && (<span>Esse campo é obrigatório</span>)}
                      </label>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 3 && (
                <>
                  <ContenInformations>
                    <h2>Dados profissionais</h2>
                  </ContenInformations>
                  <StepWizard>
                    
                    <div className="twoForms">
                      <label>
                        Faturamento anual
                        <Field 
                          type="text" 
                          name="faturamento_anual"
                          id="faturamento_anual"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.faturamento_anual && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                      <label>
                        Atividade economica
                        <Field 
                          type="text" 
                          name="grupo_atividade_economica"
                          id="grupo_atividade_economica"
                          placeholder="Ex: Antônio Mendes"
                        />
                        {errors.atividade_economica && (<span>Esse campo é obrigatório</span>)}
                        <FontAwesomeIcon icon="lock" />
                      </label>
                    </div>
                  </StepWizard>
                </>
              )}
              {formStep == 4 && (
                <>
                  <h1>Parabéns! Você concluiu o perfil da sua empresa</h1>
                  <NavigationSteps>
                    <button className="nextStep" type="submit">Criar perfil</button>
                  </NavigationSteps>
                </>
              )}
              
            </Form>
            )}
          </Formik>
          <NavigationSteps>
            {formStep > 0 && (
              <button className="prevStep" onClick={PrevStepForm}>Voltar</button>
            )}
            {formStep < 4 && (
              <button className="nextStep" onClick={NextStepForm}>Continuar</button>
            )}
          </NavigationSteps>
        </WizardFormContent>
      </WizardContent>
    </>
  );
}