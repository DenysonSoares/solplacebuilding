import { useRouter } from "next/router";
import { BoxProject, BoxTimeLine, ContentTabs, NavBarProjetos, TimeLine, TitleProject, ProgressProject, InformationProject } from "../../../stylepages/projetos";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import 'react-accessible-accordion/dist/fancy-example.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useEffect, useMemo, useState } from "react";
import api from "../../../services/api";


export default function Projeto(){
  const router = useRouter();
  const ProjectId = router.query;
  const [dataProject, setDataProject] = useState();
  const project = useMemo( () => {
    // @ts-ignore
    return dataProject?.result?.ok || []
  }, [dataProject])
  console.log(project);

  useEffect(() => {
    api
      .post("/solplace/project/get",{
        params:{
          id: ProjectId.projeto
        } 
      })
      .then(response => setDataProject(response.data));
  }, []);

   return (
    <>
      <TitleProject>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <strong>{project.name}</strong>
      </TitleProject>
      <BoxTimeLine>
        <NavBarProjetos>
          <h1>Linha do tempo do projeto</h1>
          <button>
            Menu <FontAwesomeIcon icon="bars" />
            <nav>
              <ul>
                <li>
                  <Link href="dados-do-cliente"><a><FontAwesomeIcon icon="user" /> Dados do cliente</a></Link>
                  <div className="submenu">
                    <ul>
                      <li>
                        <Link href={`${ProjectId.projeto}/perfil-de-compra`}><a>Enviar Informações</a></Link>
                      </li>
                      <li>
                        <Link href={`${ProjectId.projeto}/anexar-documentos`}><a>Anexar Documentos</a></Link>
                      </li>
                      <li>
                        <Link href={`${ProjectId.projeto}/socios`}><a>Sócios</a></Link>
                      </li>
                      <li>
                        <Link href={`${ProjectId.projeto}/modulos`}><a>Módulos</a></Link>
                      </li>
                       <li>
                        <Link href={`${ProjectId.projeto}/inversores`}><a>Inversores</a></Link>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                  <Link href="dados-do-cliente"><a><FontAwesomeIcon icon="credit-card" /> Crédito do cliente</a></Link>
                  <div className="submenu">
                    <ul>
                      <li>
                        <Link href={`${ProjectId.projeto}/credito-do-cliente`}><a>Escolher consultas</a></Link>
                      </li>
                      <li>
                        <Link href="escolher-aprovacao"><a>Anexar Documentos</a></Link>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                  <Link href="dados-do-cliente"><a>Garantias</a></Link>
                  <div className="submenu">
                    <ul>
                      <li>
                        <Link href={`${ProjectId.projeto}/garantias`}><a>Enviar garantias</a></Link>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                  <Link href="dados-do-cliente"><a>Orçamentos</a></Link>
                </li>
                <li>
                  <Link href="dados-do-cliente"><a>Nota fiscal</a></Link>
                </li>
                <li>
                  <Link href="dados-do-cliente"><a>Cancelar projeto</a></Link>
                </li>
              </ul>
            </nav>
          </button>
        </NavBarProjetos>
        <TimeLine>
          <ProgressProject style={{width: '50%'}}></ProgressProject>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </TimeLine>
      </BoxTimeLine>
      <ContentTabs>
        <Tabs>
          <TabList>
            <Tab>
              <FontAwesomeIcon icon="user" />
              <h4>Dados básicos</h4>
            </Tab>
            <Tab>
              <FontAwesomeIcon icon="university" />
              <h4>Perfil de compra</h4>
            </Tab>
            <Tab>
              <FontAwesomeIcon icon="at" />
              <h4>Detalhes da ocupação</h4>
            </Tab>
            <Tab>
              <FontAwesomeIcon icon="dollar-sign" />
              <h4>Consultas selecionadas</h4>
            </Tab>
            <Tab>
              <FontAwesomeIcon icon="bolt" />
              <h4>Orçamento</h4>
            </Tab>
            <Tab>
              <FontAwesomeIcon icon="user" />
              <h4>Linhas de crédito selecionada</h4>
            </Tab>
          </TabList>
          <TabPanel>
              <h1>Dados do projeto</h1>
              <InformationProject>
                <div>
                  <p>
                    <strong><FontAwesomeIcon icon="dollar-sign" /> Valor previsto: </strong>
                    R$ {project.valor_previsto}
                    </p>
                  <p>
                    <strong><FontAwesomeIcon icon="map-marker" /> Localização: </strong>
                    {project.estado_instalacao}
                  </p>
                  <p>
                    <strong><FontAwesomeIcon icon="bolt" /> Potência: </strong>
                    {project.potencia} kWp
                  </p>
                  <p>
                    <strong><FontAwesomeIcon icon="calendar" /> Criado em: </strong>
                    {project.criado_em}
                  </p>
                </div>
                <div>
                  <h4><FontAwesomeIcon icon="user" /> Dados do usuário Cliente</h4>
                  <p><strong>Nome: </strong>{project.cliente_nome}</p>
                  <p><strong>Email: </strong>{project.cliente_email}</p>
                  <p><strong>Telefone: </strong>200 kWp</p>
                </div>
              </InformationProject>
          </TabPanel>
          <TabPanel>
              <h1>Perfil de compra</h1>
              <InformationProject>
                <div>
                  <h4><FontAwesomeIcon icon="user" /> Informações básicas</h4>
                  <p><strong>Nome: </strong>João Mendes</p>
                  <p><strong>Email: </strong>Acajutiba - BA</p>
                  <p><strong>Telefone: </strong>200 kWp</p>
                  <h4><FontAwesomeIcon icon="user" /> Informações Pessoais</h4>
                  <p><strong>Nome: </strong>João Mendes</p>
                  <p><strong>Email: </strong>Acajutiba - BA</p>
                  <p><strong>Telefone: </strong>200 kWp</p>
                  <h4><FontAwesomeIcon icon="map-marker" /> Informações Pessoais</h4>
                  <p><strong>Nome: </strong>João Mendes</p>
                  <p><strong>Email: </strong>Acajutiba - BA</p>
                  <p><strong>Telefone: </strong>200 kWp</p>
                </div>
                <div>
                  <h4><FontAwesomeIcon icon="user" /> Dados do usuário Cliente</h4>
                  <p><strong>Nome: </strong>João Mendes</p>
                  <p><strong>Email: </strong>Acajutiba - BA</p>
                  <p><strong>Telefone: </strong>200 kWp</p>
                </div>
              </InformationProject>
          </TabPanel>
        </Tabs>
      </ContentTabs>
    </>
  );
}