import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ContenInformations } from "../../../components/HeaderInformation/styles";
import { OrangeButton } from "../../../components/OrangeButton";
import { BoxConjunto, BoxDocuments, BoxProject, Conjunto, TitleProject } from "../../../stylepages/projetos";
import Link from "next/link"
import { CardOverview, DashboardOverviewCards } from "../../../stylepages/home";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import 'react-accessible-accordion/dist/fancy-example.css';
import { useState } from "react";

export default function Garantias(){
  const router = useRouter();

  const [matriculaImovel, setMatriculaImovel] = useState("Arquivo pendente");

  return(
    <>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Enviar Garantias</h2>
      </ContenInformations>
      <BoxConjunto>
        <Tabs>
          <TabList>
            <Tab>
              <h4><FontAwesomeIcon icon="house-user" />Garantia de imóvel</h4>
            </Tab>
            <Tab>
              <h4><FontAwesomeIcon icon="car" />Garantia de veículo</h4>
            </Tab>
          </TabList>
          <TabPanel>
           
            <form>
              <Conjunto>
                <h1>Dados do imóvel</h1>
                <div className="twoForms">
                  <label>
                    CEP *
                    <input type="text" />
                  </label>
                  <label>
                    Estado *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Bairro *
                    <input type="text" />
                  </label>
                  <label>
                    Cidade *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Rua *
                    <input type="text" />
                  </label>
                  <label>
                    Número *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Complemento *
                    <input type="text" />
                  </label>
                  <label>
                    Tipo de imóvel *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Averbação do imóvel na matricula? *
                    <div className="twoForms">
                      <label>
                        <input type="radio" /> Sim
                      </label>
                      <label>
                        <input type="radio" /> Não
                      </label>
                    </div>
                  </label>
                  <label>
                    Valor venal do imóvel *
                    <input type="text" />
                  </label>
                </div>
              </Conjunto>
              <Conjunto>
                <section>
                  <p>RG</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Capa do IPTU</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Certidão negativa do imóvel</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Certidão negativada IPTU</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
              </Conjunto>
            </form>
          </TabPanel>
          <TabPanel>
            <form>
              <Conjunto>
                <div className="twoForms">
                  <label>
                    CEP *
                    <input type="text" />
                  </label>
                  <label>
                    Estado *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Bairro *
                    <input type="text" />
                  </label>
                  <label>
                    Cidade *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Rua *
                    <input type="text" />
                  </label>
                  <label>
                    Número *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Complemento *
                    <input type="text" />
                  </label>
                  <label>
                    Tipo de imóvel *
                    <input type="text" />
                  </label>
                </div>
                <div className="twoForms">
                  <label>
                    Averbação do imóvel na matricula? *
                    <input type="radio" /> Sim
                    <input type="radio" /> Não
                  </label>
                  <label>
                    Valor venal do imóvel *
                    <input type="text" />
                  </label>
                </div>
              </Conjunto>
              <Conjunto>
                <section>
                  <p>RG</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Capa do IPTU</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Certidão negativa do imóvel</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
                <section>
                  <p>Certidão negativada IPTU</p>
                  <p>{matriculaImovel}</p>
                  <input onChange={(event) => setMatriculaImovel(event.target.files[0].name)} type="file" required/>
                </section>
              </Conjunto>
            </form>
          </TabPanel>
        </Tabs>
      </BoxConjunto>
    </>
  );
}