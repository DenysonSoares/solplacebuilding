import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ContenInformations } from "../../../components/HeaderInformation/styles";
import { OrangeButton } from "../../../components/OrangeButton";
import { BoxDocuments, BoxProject, TitleProject, BoxConjunto, Conjunto } from "../../../stylepages/projetos";
import Link from "next/link"
import { CardOverview, DashboardOverviewCards } from "../../../stylepages/home";

export default function CreditoDoCliente(){
  const router = useRouter();

  return(
    <div>
      <ContenInformations>
        <button onClick={() => router.back()} className="backButton">Voltar</button>
        <h2>Escolher consultas</h2>
        
      </ContenInformations>
      <BoxConjunto>
        <Conjunto>
          <h1>Conjuto de proposta 1</h1>
          <p>Você pode selecionar apenas uma das consultas de cada conjunto</p>
          <table>
            <tbody>
              <tr>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
              </tr>
              <tr>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
              </tr>
            </tbody>
          </table>
        </Conjunto>
      </BoxConjunto>
      <BoxConjunto>
        <Conjunto>
          <h1>Conjuto de proposta 2</h1>
          <p>Você pode selecionar apenas uma das consultas de cada conjunto</p>
          <table>
            <tbody>
              <tr>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
              </tr>
              <tr>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
                <td>
                  <h4>Valor das parcelas</h4>
                  <p>R$ 6,66 por mês</p>
                </td>
              </tr>
            </tbody>
          </table>
        </Conjunto>
      </BoxConjunto>
    </div>
  );
}