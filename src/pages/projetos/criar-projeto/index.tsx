import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OrangeButton } from "../../../../src/components/OrangeButton";
import { BoxProject, ContentTabs } from "../../../../src/stylepages/projetos";
import { GlobalStyle } from "../../../../styles/globals";
import { FormEvent, useContext, useState } from "react";
import { BasicProjectsContext } from "../../../contexts/ProjetcsProvider/ProjectContext";
import  Router  from "next/router";
import { AuthContext } from "../../../contexts/AuthProvider/AuthContext";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';

export default function CriarProjeto() {
  const user = useContext(AuthContext);

  const {createBasicProject} = useContext(BasicProjectsContext); 

  const newProjectValidate = yup.object().shape({
    name: yup.string().required(),
    valor_previsto: yup.number().required(),
    potencia: yup.number().required(),
    cidade_instalacao: yup.string().required(),
    estado_instalacao: yup.string().required(),
    client_name: yup.string().required(),
    email: yup.string().email().required(),
    telefone: yup.string().min(11).required(),
  })

  return (
    <>
      <ContentTabs>
        <BoxProject>
          <h2>Antes de continuar</h2>
          <p><strong>ATENÇÃO:</strong> Entraremos em contato com seu cliente ao longo do processo. É importante lembra-lo disso.</p>
          <p>É de suma importância que o e-mail inserido seja um e-mail válido, ativo e que o cliente tenha acesso. Ele receberá todas as informações necessárias via e-mail bem como um novo cadastro (caso ainda não o tenha) em nossa plataforma.</p>
        </BoxProject>
        <BoxProject>
          <Formik
            validationSchema={newProjectValidate}
            initialValues={{
              name: '',
              valor_previsto: 0,
              potencia: 0,
              cidade_instalacao: '',
              estado_instalacao: '',
              client_name: '',
              email: '',
              telefone: '',
              localizacao: "São Paulo",
              integrador_id: user.admin_id
            }}
            onSubmit={async (values) => {
              await createBasicProject(values);
            }}
          >  
          {({ errors }) => (
          <Form>
            <label>
              Nome do projeto *
              <Field 
                type="text"
                name="name"
                id="name"
                placeholder="Ex: Sol place"
              />
              {errors.name && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="bolt" />
            </label>
            <label>
              Valor estimado do projeto *
              <Field 
                type="text" 
                name="valor_previsto"
                id="valor_previsto"
                placeholder="Ex: 2000"
              />
              {errors.valor_previsto && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="dollar-sign" />
            </label>
            <label>
              Potência estimada do projeto (kWp) *
              <Field 
                type="text" 
                name="potencia"
                id="potencia"
                placeholder="130"
              />
              {errors.potencia && (<span>Esse campo é obrigatório</span>)}
              <FontAwesomeIcon icon="bolt" />
            </label>
            <div className="twoForms">
              <label>
                Estado
                <Field
                  component="select"
                  id="estado_instalacao"
                  name="estado_instalacao"
                >
                  <option value="Acre">Acre</option>
                  <option value="Alagoas">Alagoas</option>
                  <option value="Amapá">Amapá</option>
                  <option value="Amazonas">Amazonas</option>
                  <option value="Bahia">Bahia</option>
                  <option value="Ceará">Ceará</option>
                  <option value="Distrito Federal">Distrito Federal</option>
                  <option value="Espírito Santo">Espírito Santo</option>
                  <option value="Goiás">Goiás</option>
                  <option value="Maranhão">Maranhão</option>
                  <option value="Mato Grosso">Mato Grosso</option>
                  <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                  <option value="Minas Gerais">Minas Gerais</option>
                  <option value="Pará">Pará</option>
                  <option value="Paraíba">Paraíba</option>
                  <option value="Paraná">Paraná</option>
                  <option value="Pernambuco">Pernambuco</option>
                  <option value="Piauí">Piauí</option>
                  <option value="Rio de Janeiro">Rio de Janeiro</option>
                  <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                  <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                  <option value="Rondônia">Rondônia</option>
                  <option value="Roraima">Roraima</option>
                  <option value="Santa Catarina">Santa Catarina</option>
                  <option value="São Paulo">São Paulo</option>
                  <option value="Sergipe">Sergipe</option>
                  <option value="Tocantins">Tocantins</option>
                </Field>
                {errors.estado_instalacao && (<span>Esse campo é obrigatório</span>)}
              </label>
              <label>
                Cidade
                <Field 
                  type="text" 
                  name="cidade_instalacao"
                  id="cidade_instalacao"
                  placeholder="Ex: Uberaba"
                />
                {errors.cidade_instalacao && (<span>Esse campo é obrigatório</span>)}
              </label>
            </div>
            <label>
                Nome do cliente *
                <Field 
                  type="text" 
                  name="client_name"
                  id="client_name"
                  placeholder="Ex: Antônio Mendes"
                />
                {errors.client_name && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="user" />
              </label>
              <label>
                E-mail do cliente *
                <Field 
                  type="text" 
                  name="email"
                  id="email"
                  placeholder="Ex: antonio@gmail.com"
                />
                {errors.email && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="at" />
              </label>
              <label>
                Celular do cliente *
                <Field 
                  type="text" 
                  name="telefone"
                  id="telefone"
                  placeholder="(00)00000-0000"
                />
                {errors.telefone && (<span>Esse campo é obrigatório</span>)}
                <FontAwesomeIcon icon="phone" />
              </label>
              <OrangeButton type="submit" buttontitle="Criar projeto" iconname="plus"/>
            </Form>
            )}
          </Formik>
        </BoxProject>
      </ContentTabs>
    </>
  );
}
