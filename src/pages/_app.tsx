import { GlobalStyle } from "../../styles/globals";
import Modal from "react-modal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { MainContent } from "../../src/components/wrapperContainer/styles";
import { Navbar } from "../../src/components/wrapperContainer/NavBar";
import { PageContent } from "../../src/components/wrapperContainer/Content/styles";
import { useState } from "react";
import { IconNotification, ImageLogo, TopBarFixed } from "../../src/stylepages/navigation";
import  LogoSolPlaceIcon  from "../../public/images/logo-solplace-icon.png";
import  LogoSolPlaceSidebar  from "../../public/images/logo-solplace-sidebar.png";
import Image from "next/image";
import { AppProvider } from "../../src/contexts/AllProviders";
import ProtectedProfile from "../components/ProtectedProfile";
import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

const options = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: '30px',
  transition: transitions.SCALE
}

Modal.setAppElement('#__next');

function MyApp({ Component, pageProps }) {
  const [opencontent, setOpencontent] = useState(false);
  function PushContent(){
    setOpencontent(!opencontent);
  }
  switch (Component.name) {
    case "Start":
      return (
        <AlertProvider template={AlertTemplate} {...options}>
          <GlobalStyle />
          <Component {...pageProps} />
        </AlertProvider>
    );
    case "StartLogin":
      return (
        <AlertProvider template={AlertTemplate} {...options}>
          <GlobalStyle />
          <AppProvider>
            <Component {...pageProps} />
          </AppProvider>
        </AlertProvider>
      );
    default:
      return (
        <AlertProvider template={AlertTemplate} {...options}>
          <GlobalStyle />
          <AppProvider>
            <ProtectedProfile>
              <MainContent>
                <Navbar />
                <PageContent style={{marginLeft: opencontent ? "90px" : "270px" }}>
                  <TopBarFixed>
                    <ImageLogo style={{width: opencontent ? "90px" : "270px" }}>
                      {opencontent ? <Image src={LogoSolPlaceIcon} alt="Logo SolPlace" /> : <Image src={LogoSolPlaceSidebar} alt="Logo SolPlace" /> }
                    </ImageLogo>
                    <button onClick={PushContent}><FontAwesomeIcon icon="bars" /></button>
                    <IconNotification>
                      <FontAwesomeIcon icon="bell" />
                    </IconNotification>
                  </TopBarFixed>
                  <Component {...pageProps} />
                </PageContent>
              </MainContent> 
            </ProtectedProfile>
          </AppProvider>
        </AlertProvider>
      );
  }
}

export default MyApp;
