import Head from "next/head";
import Image from "next/image";
import Script from "next/script";
import Link from "next/link";
import LogoSolPlaceStart from "../../public/images/logo-solplace-start.png";
import { OrangeButton } from "../../src/components/OrangeButton";
import { ContentStart } from "../../src/stylepages/start";

export default function Start() {
  return (
    <ContentStart>
      <Image src={LogoSolPlaceStart} alt="Logo Sol Place"/>
      <Link href="/login/">
        <a>
          <OrangeButton buttontitle="Entrar" />
        </a>
      </Link>
    </ContentStart>
  );
}
