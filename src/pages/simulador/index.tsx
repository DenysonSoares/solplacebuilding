import { FormEvent, useContext, useState } from "react";
import { BoxProject } from "../../../src/stylepages/projetos";
import { ContentSimulator, ValueParting } from "../../../src/stylepages/simulador";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OrangeButton } from "../../../src/components/OrangeButton";
import { SimulatorsContext } from "../../contexts/SimulatorProvider/SimulatorContext";
import  Router  from "next/router";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";

export default function Simulador() {
  const user = useContext(AuthContext)
  const {createSimulator} = useContext(SimulatorsContext);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [telefone, setTelefone] = useState(0);
  const [preco, setPreco] = useState(0);
  const [entrada, setEntrada] = useState (0);
  const [tipo_cliente, setTipoCliente] = useState ('');
  const [numero_parcela_sem_garantia, setNumeroParcelasSemGarantia] = useState (0);
  const [numero_parcela_veiculo, setNumeroParcelasVeiculo] = useState (0);
  const [numero_parcela_imovel, setNumeroParcelasImovel] = useState (0);
  const [tipo_conexao, setTipoConexao] = useState ('');
  const [consumo_medio, setConsumoMedio] = useState (0);
  const [dimensionado, setDimensionado] = useState (0);
  const [state, setState] = useState ("analise");
  const [estado, setEstado] = useState ('');
  const [partner_id, setPartnerId] = useState ("Jeff");

  async function handleNewSimulator(event: FormEvent) {
    event.preventDefault();

    await createSimulator({
      name,
      email,
      telefone,
      preco,
      entrada,
      tipo_cliente,
      numero_parcela_sem_garantia,
      numero_parcela_veiculo,
      numero_parcela_imovel,
      tipo_conexao,
      consumo_medio,
      dimensionado,
      state,
      estado,
      partner_id
    })
    console.log(name)
    

    Router.push('/simulador');
  }

  return (
    <ContentSimulator>
      <form onSubmit={handleNewSimulator}>
        <BoxProject>
          <h2><strong>  Realizar simulação</strong></h2>
          <p>Para alterar sua senha preencha todos os campos abaixo</p>
          <div>
            <div className="twoForms">
              <label>
                Preço *
                <input
                type="text" 
                value={preco} 
                onChange={event => setPreco(Number(event.target.value))}
                required/>
                <FontAwesomeIcon icon="dollar-sign" />
              </label>
              <label>
                Entrada *
                <input
                type="text" 
                value={entrada} 
                onChange={event => setEntrada(Number(event.target.value))}
                required/>
                <FontAwesomeIcon icon="dollar-sign" />
              </label>
              <label>
                Carência *
                <input
                type="text" 
                value={estado} 
                onChange={event => setEstado(event.target.value)}
                required/>
              </label>
            </div>
          </div>
          <label>
            Número de parcelas para financiamento sem garantia*
            <div className="rangeContent">
              <div className="rangeBox">
                <strong>{numero_parcela_sem_garantia}</strong>
                  <input 
                    type="range" 
                    min="0" 
                    max="120"
                    value={numero_parcela_sem_garantia}
                    onChange={(event) => setNumeroParcelasSemGarantia(Number(event.target.value))} 
                  />
                <strong>120</strong>
              </div>
              <ValueParting>
                <p>
                  Valor da parcela: 
                  <strong>R$: 3.000.00</strong>
                </p>
              </ValueParting>
            </div>
          </label>
          <label>
            Número de parcelas para financiamento com garantia de veículo*
            <div className="rangeContent">
              <div className="rangeBox">
                <strong>{numero_parcela_veiculo}</strong>
                  <input 
                    type="range" 
                    min="0" 
                    max="60"
                    value={numero_parcela_veiculo}
                    onChange={(event) => setNumeroParcelasVeiculo(Number(event.target.value))} 
                  />
                <strong>60</strong>
              </div>
              <ValueParting>
                <p>
                  Valor da parcela: 
                  <strong>R$: 3.000.00</strong>
                </p>
              </ValueParting>
            </div>
          </label>
          <label>
            Número de parcelas para financiamento com garantia de imóvel*
            <div className="rangeContent">
             <div className="rangeBox">
                <strong>{numero_parcela_imovel}</strong>
                  <input 
                    type="range" 
                    min="0" 
                    max="120"
                    value={numero_parcela_imovel}
                    onChange={(event) => setNumeroParcelasImovel(Number(event.target.value))} 
                  />
                <strong>120</strong>
              </div>
              <ValueParting>
                <p>
                  Valor da parcela: 
                  <strong>R$: 3.000.00</strong>
                </p>
              </ValueParting>
            </div>
            <OrangeButton buttontitle="Calcular"/>
          </label>
          <label>
            Nome do cliente
            <input
            type="text" 
            value={tipo_conexao} 
            onChange={event => setTipoConexao(event.target.value)}
            required/>
          </label>
          <label>
            E-mail do cliente
            <input
            type="text" 
            value={consumo_medio} 
            onChange={event => setConsumoMedio(Number(event.target.value))}
            required/>
          </label>
          <label>
            Telefone do cliente
            <input
            type="text" 
            value={dimensionado} 
            onChange={event => setDimensionado(Number(event.target.value))}
            required/>
          </label>
          <OrangeButton buttontitle="Realizar Simulação" onClick={handleNewSimulator}/>
        </BoxProject>
      </form>
    </ContentSimulator>
  );
}
