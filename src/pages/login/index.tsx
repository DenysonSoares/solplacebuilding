import { Login } from "../../../src/components/Login/login";
import { Presentation } from "../../../src/components/Login/presentation";
import { WrapperPresentationLogin } from "../../../src/components/Login/styles";
import { useAuth } from "../../contexts/AuthProvider/useAuth";


export default function StartLogin() {
  return (
    <WrapperPresentationLogin>
      <Presentation />
      <Login />
    </WrapperPresentationLogin>
  );
}