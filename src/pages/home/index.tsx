import Image from "next/image";
import Link from "next/link";
import { ContenInformations, InformationDate } from "../../../src/components/HeaderInformation/styles";
import { DashboardOverviewCards, CardOverview, InformationSimulator} from "../../../src/stylepages/home"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { getUserLocalStorage } from "../../contexts/AuthProvider/util";
import { useContext, useEffect, useState } from "react";
import { Console } from "console";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";
library.add(fas);

export default function Home(props) {
  const user = useContext(AuthContext);

  return (
    <div>
      <ContenInformations>
        <h1>Bem vindo, <strong>{user.name}</strong></h1>
        <InformationDate>
          Você esta genrenciando 
          <p>Último acesso sexta feira, 20 de agosto de 2021 às 20h30</p>
        </InformationDate>
      </ContenInformations>
      <DashboardOverviewCards>
        <CardOverview>
          <h4><FontAwesomeIcon icon="bolt" /> Orçamentos</h4>
          <article>
            <h1>34</h1>
            <Link href="#"><a>Acessar orçamentos <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
        <CardOverview>
          <h4><FontAwesomeIcon icon="bolt" /> Projetos</h4>
          <article>
            <h1>{user.numero_projetos}</h1>
            <Link href="/projetos"><a>Acessar projetos <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
      </DashboardOverviewCards>
      <InformationSimulator>
        <h1>Simulação de financiamentos para projeto fotovoltaico</h1>
        <p>Para prosseguir com o processo de compra do seu sistema fotovoltaico você deve fornecer algumas informações a respeito de você ou de sua empresa</p>
      </InformationSimulator>
      <DashboardOverviewCards>
         <CardOverview>
          <h4><FontAwesomeIcon icon="bolt" /> Baixa Tensão</h4>
          <article>
            <h1>34</h1>
            <Link href="/simulador"><a className="orange-button">Ir para Simulação <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
        <CardOverview>
          <h4><FontAwesomeIcon icon="bolt" /> Média Tensão</h4>
          <article>
            <h1>34</h1>
            <Link href="#"><a className="orange-button disabled">Ainda não disponível <FontAwesomeIcon icon="chevron-right" /></a></Link>
          </article>
        </CardOverview>
      </DashboardOverviewCards>
    </div>
  );
}
