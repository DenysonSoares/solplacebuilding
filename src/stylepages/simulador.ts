import styled from "styled-components";

export const ContentSimulator = styled.div`
  margin-top: 50px;
  div{
    p{
      margin-top: 20px;
      margin-bottom: 50px
    }
  }
`

export const ValueParting = styled.div`
  text-align: center;
  border-left: 1px solid #B6B6B6;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  p{
    display: block;
    font: 16px;
    strong{
      display: block;
      margin-top: 5px;
    }
  }
`