import styled from "styled-components";
import { Colors, Paddings, Fonts } from "../../styles/variables"

export const WizardContent = styled.div`
  display: grid;
  grid-template-columns: 25% 75%;
  height: 100%;
`;

export const WizardNav = styled.div`
  background: #fefefe;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  height: 100vh;
  padding: 10% 15%;
  clear: both;
  h1 {
    color: #232323;
    margin-bottom: 15px;
  }
  p {
    color: #59c85d;
    strong {
      color: #232323;
    }
  }
`;
export const NavButtons = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: flex-start;
  position: relative;
  margin-top: 60px;
`

export const ButtonChecked = styled.button`
  width: 35px;
  height: 35px;
  box-sizing: border-box;
  border-radius: 50%;
  font-size: ${Fonts.Paragrapher};
  position: relative;
  border: 0;
  background-color: transparent;
  &.noActive{
    background-color: transparent;
    border: 1px solid #A2A2A2;
    color: #A2A2A2;
  }
  &.inWrite{
    background-color: transparent;
    border: 2px solid #061887;
    color: #061887;
    font-weight: 600;
  }
   &.inChecked{
    background-color: #59C85D;
    border: 2px solid #59C85D;
    color: #ffffff;
    font-weight: 600;
    :after{
      color: #59C85D;
    }
    :before{
      background-color: #59C85D !important;
    }
  }
  &:not(:last-child){
    margin-bottom: 50px;
  }
  :after{
    padding-left: 30px;
    position: absolute;
    width: max-content;
  }
  :nth-child(1):after{
    content: "Dados Básicos";
    padding-left: 30px;
  }
  :nth-child(2):after{
    content: "Endereço";
    padding-left: 30px;
  }
  :nth-child(3):after{
    content: "Dados bancários";
    padding-left: 30px;
  }
  :nth-child(4):after{
    content: "Responsável técnico";
    padding-left: 30px;
  }
  :nth-child(5):after{
    content: "Dados técnicos";
    padding-left: 30px;
  }
  :nth-child(6):after{
    content: "Atualizar";
    padding-left: 30px;
  }
  :not(:last-child):before{
    content: "";
    width: 3px;
    height: 35px;
    background-color: #A2A2A2;
    position: absolute;
    left: 15px;
    top: 40px;
  }
`

export const WizardFormContent = styled.div`
  width: 100%;
`

export const StepWizard = styled.div`
  padding: ${Paddings.paddingContainer};
`

export const NavigationSteps = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${Paddings.paddingSides};
  button{
    padding: 10px 20px;
    &.nextStep{
      background-color: ${Colors.orangeDefault};
      box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
      border-radius: 5px;
      border: 0;
      color: #ffffff;
      font-size: ${Fonts.Paragrapher};
    }
    &.prevStep{
      background-color: #FEFEFE;
      padding: 10px 20px;
      border: 0.5px solid rgba(87, 87, 87, 0.5);
      box-sizing: border-box;
      box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
      border-radius: 5px;
      font-size: ${Fonts.Paragrapher};
      color: #232323;
      font-weight: 500;
      margin-right: 10px;
    }
  }
`