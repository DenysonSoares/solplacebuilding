import styled from "styled-components";
import { Colors, Paddings, Fonts } from "../../styles/variables";

export const DashboardOverviewCards = styled.div`
  padding: ${Paddings.paddingContainer};
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 4%;
  overflow: hidden;
`;

export const CardOverview = styled.div`
  background: #fefefe;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  display: inline-table;
  margin-bottom: 20px;
  h4 {
    color: ${Colors.orangeDefault};
    text-align: center;
    padding: 20px;
    border-bottom: 1px solid rgba(35, 35, 35, 0.2);
  }
  article {
    padding: 30px 20px;
    text-align: center;
    h1 {
      font-size: 40px;
      text-align: center;
      color: #232323;
      padding: 0px 20px 30px 20px;
      svg{
        font-size: 50px;
      }
    }
    a{
      color: #232323;
      font-size: 16px;
      transition: all 0.2s;
      &:hover{
        transition: all 0.2s;
        color: ${Colors.orangeDefault};
      }
      &.orange-button{
        background-color: ${Colors.orangeDefault};
        padding: 5px 25px;
        border-radius: 4px;
        color: #ffffff;
        font-size: 13px;
      }
      &.disabled{
        background-color: #A2A2A2;
        padding: 5px 25px;
        border-radius: 4px;
        color: #ffffff;
        font-size: 13px;
        pointer-events: none;
      }
    }
  }
`;

export const InformationSimulator = styled.div`
  background: #fff8f8;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  padding: ${Paddings.paddingContainer};
  height: max-content;
  h1 {
    color: ${Colors.orangeDefault};
    margin-bottom: 20px;
  }
  p {
    color: #575757;
    font-weight: 500;
    font-size: 18px;
    line-height: 21px;
  }
`;
