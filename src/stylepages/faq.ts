import styled from "styled-components";
import { Colors } from "../../styles/variables"

export const SearchAsc = styled.div`
  margin: auto;
  width: max-content;
  text-align: center;
  margin-top: 4%;
  h1{
    margin-bottom: 20px;
  }
  div{
    margin: 0;
  }
`
export const AskContent = styled.div`
  .react-tabs__tab-list{
    text-align: center;
    border: 0;
     display: grid;
    grid-template-columns: repeat(7,  1fr);
    grid-gap: 1%;
    padding: 0 4%;
    padding-top: 6%;
    margin-bottom: 20px;
  }
  .react-tabs__tab{
    padding: 30px 15px;
    background-color: #ffffff;
    border-radius: 10px;
    background: #FEFEFE;
    color: #575757;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
    border-radius: 8px;
    svg{
      font-size: 3.75em;
      margin-bottom: 10px;
    }
  }
  .react-tabs__tab--selected{
    text-align: center;
    background-color: ${Colors.orangeDefault};
    border: 1px solid ${Colors.orangeDefault};
    color: #ffffff;
  }
  .react-tabs__tab-panel{
    padding: 0px 4%;
  }
  .accordion__item{
    background-color: #FEFEFE;
    border-radius: 10px;
    margin-bottom: 10px;
  }
  .accordion__item + .accordion__item{
    border: 0;
  }
  .accordion__button{
    border-radius: 10px;
    border-left: 4px solid ${Colors.orangeDefault};
    margin-bottom: 10px;
  }
  .accordion__button:before{
    color: ${Colors.orangeDefault};
  }
`