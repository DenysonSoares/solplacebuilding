import styled from "styled-components";
import { Colors } from "../../styles/variables"

export const ContentStart = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${Colors.blueDefault};
  row-gap: 5%;
`