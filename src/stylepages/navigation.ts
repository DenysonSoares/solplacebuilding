import styled from "styled-components";
export const TopBarFixed = styled.div`
  position:fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 80px;
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  display: flex;
  align-items: center;
  justify-content: flex-start;
  z-index: 9;
  button{
    color: #061887;
    font-size: 1.875em;
    background: transparent;
    border: 0;
    margin-left: 30px;
  }
`

export const ImageLogo = styled.div`
  height: 80px;
  background: #061887;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  transition: 0.5s;
  padding-left: 30px;
`

export const IconNotification = styled.div`
  position: absolute;
  right: 30px;
`