import styled from "styled-components";
import { Colors, Fonts, Paddings } from "../../styles/variables";

// PROJETOS
export const ContentTopUsuários = styled.div`
  background: #fefefe;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${Paddings.paddingContainer};
  button {
    background-color: ${Colors.orangeDefault};
    padding: 10px 20px;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    border: 0;
    color: #ffffff;
    font-size: ${Fonts.Paragrapher};
    svg {
      margin-right: 10px;
    }
  }
`;

// CRIAR PROJETO
export const ContentTabs = styled.div`
  padding: ${Paddings.paddingContainer};
  .react-tabs__tab-list{
    text-align: center;
    border: 0;
    display: grid;
    grid-template-columns: repeat(6,  1fr);
    grid-gap: 1%;
    margin-bottom: 20px;
  }
  .react-tabs__tab{
    padding: 15px 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: left;
    color: #575757;
    background: #FEFEFE;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
    border-radius: 8px;
    h4{
      text-align: left;
      font-size: 14px;
      font-weight: 600;
    }
    svg{
      font-size: 1.2em;
      margin-bottom: 2px;
      margin-right: 10px;
    }
  }
  .react-tabs__tab--selected{
    text-align: center;
    background-color: ${Colors.orangeDefault};
    border: 1px solid ${Colors.orangeDefault};
    color: #ffffff;
  }
  .react-tabs__tab-panel{
    padding: 2%;
    background: #FEFEFE;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
    border-radius: 16px;
    h1{
      color: #575757;
      margin-bottom: 30px;
    }
    h4{
      font-size: 18px;
      color: #575757;
      font-weight: 600;
      margin: 20px 0px;
    }
    p{
      font-size: 16px;
      color: #575757;
      margin-bottom: 15px;
    }
  }
  .accordion__item{
    background-color: #FEFEFE;
    border-radius: 10px;
    margin-bottom: 10px;
  }
  .accordion__item + .accordion__item{
    border: 0;
  }
  .accordion__button{
    border-radius: 10px;
    border-left: 4px solid ${Colors.orangeDefault};
    margin-bottom: 10px;
  }
  .accordion__button:before{
    color: ${Colors.orangeDefault};
  }
`

export const BoxProject = styled.div`
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 15px;
  padding: 2%;
  max-width: 900px;
  margin: auto;
  margin-bottom: 2%;
  h1{
    margin-bottom: 20px;
    color: #575757;
  }
  p{
    margin-bottom: 15px;
    color: #575757;
  }
`

// PROJETOS INTERNA
export const TitleProject = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding: 2%;
  strong{
    text-align: center;
    color: #FF6900;
    font-size: 32px;
    text-transform: capitalize;
  }
`
export const BoxTimeLine = styled.div`
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 16px;
  margin: 2%;
  padding: 2%;
  min-height: 450px;
  h1{
    color: #575757;
  }
`
export const NavBarProjetos = styled.div`
  display: flex;
  justify-content: space-between;
  button{
    background: #FEFEFE;
    border: 0.5px solid rgba(0, 0, 0, 0.5);
    box-sizing: border-box;
    border-radius: 8px;
    padding: 0px 15px;
    position: relative;
    &:hover{
      nav{
        display: block;
        transition: 0.4s all;
      }
    }
    nav{
      display: none;
      position: absolute;
      width: max-content;
      top: 35px;
      right: 0;
      background: #FEFEFE;
      border: 0.5px solid rgba(87,87,87,0.5);
      box-sizing: border-box;
      box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
      border-radius: 8px;
      transition: 0.4s all;
      z-index:9;
      padding: 10px 0px;
      ul{
        list-style: none;
        li{
          padding: 10px 20px;
          display: block;
          width: 100%;
          text-align: left;
          &:hover{
            .submenu{
              display: block;
            }
          }
          .submenu{
            position: absolute;
            top: 0;
            width: 200px;
            left: -200px;
            background: #FEFEFE;
            border: 0.5px solid rgba(87,87,87,0.5);
            box-sizing: border-box;
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
            border-radius: 8px 0px 0px 8px;
            transition: 0.4s all;
            z-index:9;
            padding: 10px 0px;
            display: none;
          }
          a{
            color: #575757;
            font-size: 16px;
            font-weight: 600;
            text-align: left;
          }
        }
      }
    }
  }
`

export const TimeLine = styled.div`
  width: 100%;
  height: 6px;
  background: linear-gradient(90deg, #FF8500 0%, #381BD3 51.12%, #11A4C4 100%);
  position: relative;
  margin-top: 200px;
  border-radius: 10px;
  span{
    display: block;
    height: 35px;
    width: 12px;
    border-radius: 10px;
    background-color: #061887;
    position: absolute;
    top: -15px;
    left: 4%;
    ::before{
      content: "Cadastro do comprador";
      position: absolute;
      bottom: -130px;
    }
    ::after{
        content: "";
        width: 4px;
        height: 50px;
        border-left: 2px dashed #E4E4E4;
        position: absolute;
        bottom: -65px;
        left: 4px;
      }
    &:nth-child(2){
      left: 18%;
      ::before{
        content: "Perfil do comprador";
        position: absolute;
        top: -130px;
      }
      ::after{
        content: "";
        width: 4px;
        height: 80px;
        border-left: 2px dashed #E4E4E4;
        position: absolute;
        top: -90px;
        left: 4px;
      }
    }
     &:nth-child(3){
      left: 33%;
      ::before{
        content: "Análise preliminar de crédito";
        position: absolute;
      }
    }
    &:nth-child(4){
      left: 49%;
      ::before{
        content: "Documentos";
        position: absolute;
        top: -100px;
      }
      ::after{
        content: "";
        width: 4px;
        height: 65px;
        border-left: 2px dashed #E4E4E4;
        position: absolute;
        top: -70px;
        left: 4px;
      }
    }
    &:nth-child(5){
      left: 65%;
      ::before{
        content: "Orçamento";
        position: absolute;
        bottom: -90px
      }
    }
    &:nth-child(6){
      left: 81%;
      ::before{
        content: "Ofertas";
        position: absolute;
        top: -90px;
      }
      ::after{
        content: "";
        width: 4px;
        height: 60px;
        border-left: 2px dashed #E4E4E4;
        position: absolute;
        left: 4px;
        top: -65px;
      }
    }
    &:nth-child(7){
      left: 95%;
      ::before{
        content: "Nota fiscal";
        position: absolute;
      }
    }
  }
`

export const ProgressProject = styled.div`
  height: 6px;
  position: absolute;
  left: 0;
  top: 0;
  border-radius: 10px;
  background: orange;
`

export const InformationProject = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

// DOCUMENTOS
export const BoxDocuments = styled.div`
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 15px;
  padding: 2%;
  margin: 2% 6%;
  margin-bottom: 2%;
  h5{
    font-size: 22px;
    color: #232323;
    margin-bottom: 20px;
  }
  p{
    color: #575757;
  }
  form{
    margin-top: 30px;
    section{
      display: grid;
      grid-template-columns: 2fr 1fr 1fr;
      align-items: center;
      justify-content:space-between;
      padding: 10px 0px;
      border-top: 1px solid #A2A2A2;
      color: #575757;
      &:last-child{
        margin-bottom: 20px;
      }
      input[type=file]{
        visibility: hidden;
        height: 40px;
        overflow: inherit;
        float: right;
        &:placeholder-shown {
          background-color:red;
        }
        :before{
          visibility: visible;
          content: "Enviar +";
          font-weight: 600;
          color: #061887;
          background: #FEFEFE;
          border: 1px solid #061887;
          box-sizing: border-box;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
          padding: 10px 30px;
          z-index: 1;
          float: right;
        }
      }
    }
  }
`

// CREDITO DO CLIENTE
export const BoxConjunto = styled.div`
  padding: 0 6%;
  margin: 40px 0px;
  .react-tabs__tab-list{
    text-align: center;
    border: 0;
    display: grid;
    grid-template-columns: repeat(6,  1fr);
    grid-gap: 1%;
    margin-bottom: 20px;
  }
  .react-tabs__tab{
    padding: 15px 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: left;
    color: #575757;
    background: #FEFEFE;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
    border-radius: 8px;
    h4{
      text-align: left;
      font-size: 14px;
      font-weight: 600;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    svg{
      font-size: 1.4em;
      margin-bottom: 2px;
      margin-right: 10px;
    }
  }
  .react-tabs__tab--selected{
    text-align: center;
    background-color: ${Colors.orangeDefault};
    border: 1px solid ${Colors.orangeDefault};
    color: #ffffff;
  }
  .react-tabs__tab-panel{
    h1{
      color: #575757;
      margin-bottom: 30px;
    }
    h4{
      font-size: 18px;
      color: #575757;
      font-weight: 600;
      margin: 20px 0px;
    }
    p{
      font-size: 16px;
      color: #575757;
      margin-bottom: 15px;
    }
  }
`


export const Conjunto = styled.div`
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 15px;
  width: 100%;
  padding: 2%;
  margin-bottom: 30px;
  h1{
    margin-bottom: 20px;
  }
  table{
    margin-top: 20px;
    width: 100%;
    tr:first-child{
      td:nth-child(1),
      td:nth-child(2),
      td:nth-child(3),
      td:nth-child(4){
        border-bottom: 1px solid #e5e5e5;
      }
    }
    tr{
      td:nth-child(1n + 2){
        padding: 20px 30px;
        border-left: 1px solid #e5e5e5;
      }
    }
  }
`

// SOCIOS
export const DashboardSocios = styled.div`
  padding: ${Paddings.paddingContainer};
` 
export const ListSocios = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 4%;
  overflow: hidden;
  margin-top: 30px;
`

export const CardSocio = styled.div`
  background: #FEFEFE;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
  border-radius: 15px;
  padding: 25px;
  header{
    display: flex;
    align-items: center;
    justify-content: space-between;
    border-bottom: 1px solid #EFF1FE;
    padding-bottom: 15px;
    margin-bottom: 15px;
    font-size: 22px;
    color: #575757;
    button{
      background-color: transparent;
      border: none;
      font-weight: bold;
      font-size: 22px;
      color: #575757;
      transform: rotate(90deg);
    }
  }
  article{
    p{
      margin-bottom: 10px;
      color: #575757;
      font-size: 16px;
    }
  }
`