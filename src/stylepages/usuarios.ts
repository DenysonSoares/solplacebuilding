import styled from "styled-components";
import { Colors, Paddings } from "../../styles/variables";

export const ContentTopUsuários = styled.div`
  background: #fefefe;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${Paddings.paddingContainer};
  button {
    background-color: ${Colors.orangeDefault};
    padding: 10px 20px;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    border: 0;
    color: #ffffff;
    font-size: 18px;
    svg {
      margin-right: 10px;
    }
  }
`;



export const ContentModal = styled.div`
  h1{
    text-align: center;
    color: #FF6900;
    font-size: 32px;
  }
  p{
    text-align: center;
    font-size: 16px;
    color: #575757;
    margin-bottom: 35px;
  }
  form{
    label{
      color: #575757;
      font-weight: 500;
      position: relative;
      display: block;
      svg{
        position: absolute;
        top: 40px;
        right: 20px;
      }
      input{
        width: 100%;
        height: 50px;
        background: #FEFEFE;
        border: 1px solid #B6B6B6;
        box-sizing: border-box;
        border-radius: 5px;
        margin-bottom: 25px;
      }
    }
  }
`

export const ActionsButtonModal = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
