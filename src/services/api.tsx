import axios from "axios";
// Pode ser algum servidor executando localmente: 
// http://localhost:3000
const api = axios.create({
  baseURL: "http://13.58.26.2:8069/api/",
});

api.interceptors.response.use(function (response) {
  // @ts-ignore
  if (response?.result?.error){
    // @ts-ignore
    throw new Error(response.result.error)
  }
  return response;
}, function (error) {
  return error;
});

export default api;