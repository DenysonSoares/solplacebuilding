import styled from "styled-components";
import { Colors, Fonts } from "../../../../styles/variables";

export const ContainerNav = styled.div`
  background-color: ${Colors.blueDefault};
  width: 270px;
  height: 100vh;
  transition: all 0.3s;
  position: relative;
  overflow: hidden;
  position: fixed;
  z-index: 1;
`;


export const NavMenu = styled.nav`
  position: fixed;
  margin-top: 80px;
  ul {
    width: 270px;
    li {
      a {
        text-decoration: none;
        color: #ffffff;
        font-size: ${Fonts.Paragrapher};
        display: block;
        padding: 20px 25px 20px 35px;
        border-right: 5px solid transparent;
        transition: 0.3s;
        z-index:9;
        display: block;
        width: 100%;
        svg {
          margin-right: 45px;
          text-align: center;
        }
        &:hover {
          background-color: #061467;
          border-color: #ff6900;
          transition: 0.3s;
          z-index:9;
        }
      }
    }
  }
`;

export const LogoutButton = styled.button`
  color: #ffffff;
  font-size: ${Fonts.Paragrapher};
  display: flex;
  width: 100%;
  text-align: left;
  padding-left: 40px;
  padding-top: 20px;
  padding-bottom: 40px;
  background-color: transparent;
  border: 0;
  position: absolute;
  bottom: 0;
  border-top: 1px solid rgba(254, 254, 254, 0.2);
  img{
    display: block;
    padding-right: 40px !important;
  }
`;
