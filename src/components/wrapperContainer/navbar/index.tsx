import { ContainerNav, NavMenu, LogoutButton } from "./styles";
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import  IconLogout  from "../../../../public/icons/icon-logout.png";
import { useAuth } from "../../../contexts/AuthProvider/useAuth";
library.add(fas);

export function Navbar() {
  const auth = useAuth();

  async function handleSignOut() {
    await auth.logout();  
  }


  return (
    <ContainerNav>
      <NavMenu>
        <ul>
          <li>
            <Link href="/home">
              <a>
                <FontAwesomeIcon icon="home" />
                Home
              </a>
            </Link>
          </li>
          <li>
            <Link href="/perfil">
              <a>
                <FontAwesomeIcon icon="building" />
                Perfil da empresa
              </a>
            </Link>
          </li>
          <li>
            <Link href="/usuarios">
              <a>
                <FontAwesomeIcon icon="user" />
                Usuários
              </a>
            </Link>
          </li>
          <li>
            <Link href="/simulador">
              <a>
                <FontAwesomeIcon icon="calculator" />
                Simulador
              </a>
            </Link>
          </li>
          <li>
            <Link href="/projetos">
              <a>
                <FontAwesomeIcon icon="bolt" />
                Projetos
              </a>
            </Link>
          </li>
           <li>
            <Link href="/alterar-senha">
              <a>
                <FontAwesomeIcon icon="pen" />
                Alterar Senha
              </a>
            </Link>
          </li>
          <li>
            <Link href="/faq">
              <a>
                <FontAwesomeIcon icon="comment-dots" />
                FAQ
              </a>
            </Link>
          </li>
        </ul>
      </NavMenu>
      <LogoutButton onClick={handleSignOut}>
        <Image src={IconLogout} alt="Logout" />
        Sair
      </LogoutButton>
    </ContainerNav>
  );
}
