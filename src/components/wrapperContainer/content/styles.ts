import styled from "styled-components";
import { Colors } from "../../../../styles/variables";

export const PageContent = styled.div`
  background-color: ${Colors.bodyBackgroundDefault};
  width: 100%;
  min-height: 100vh;
  margin-left: 267px;
  position: relative;
  box-sizing: border-box;
  margin-top: 80px;
  z-index: 8;
  transition: 0.5s;
`;
