import styled from "styled-components";

export const ContentForm = styled.div`
  margin-right: 100px;
  input {
    border: 1px solid #e8e8e8;
    box-sizing: border-box;
    border-radius: 5px;
    height: 40px;
    width: 320px;
    padding-left: 20px;
  }
`;
