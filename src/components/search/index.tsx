import { useState } from "react";
import { ContentForm } from "./styles";

export function Search(props) {

  return (
    <ContentForm>
      <input type="text" placeholder={props.nameplaceholder} />
    </ContentForm>
  );
}
