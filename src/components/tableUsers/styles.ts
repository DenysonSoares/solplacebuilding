import styled from "styled-components";
import { Paddings } from "../../../styles/variables"

export const ContentTableUser = styled.main`
  padding: ${Paddings.paddingContainer};
  /* table thead tr:last-child th:first-child {
    border-bottom-left-radius: 8px;
  }

  table thead tr:last-child th:last-child {
    border-bottom-right-radius: 8px;
  } */
  table {
    width: 100%;
    border-spacing: 0 0.5rem;
    thead {
      background-color: #575757;
      color: #ffffff;
      box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
      border-collapse: collapse;
      overflow: hidden;
      -moz-border-radius: 3px;
      -webkit-border-radius: 3px;
      border-radius: 3px;
      tr {
        border-radius: 10px;
        th {
          padding: 20px 30px;
          text-align: left;
          font-size: 16px;
          font-weight: 400;
        }
        :first-child {
          th:first-child {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
          }
          th:last-child {
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
          }
        }
      }
    }
    tbody {
      tr {
        background-color: #ffffff;
        border-radius: 8px;
        td {
          font-size: 16px;
          font-weight: 400;
          padding: 1rem 2rem;
          border: 0;
          :first-child {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
          }
          :last-child {
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
          }
        }
      }
    }
  }
`;
