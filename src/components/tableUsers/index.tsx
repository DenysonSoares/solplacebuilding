import { ContentTableUser } from "./styles"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon  } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { useContext, useEffect, useMemo, useState } from "react";
import api  from "../../services/api";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";

library.add(fas);

export function TableUsers(){
  const user = useContext(AuthContext);
  const [ users, setUsers] = useState();
  const listUsers = useMemo( () => {
    // @ts-ignore
    return users?.result?.ok || []
  }, [users])

    useEffect(() => {
    api
      .post("solplace/user/get/list",{
        params:{
          admin_id: user.admin_id,
          email:  user.email
        } 
      })
      .then(response => setUsers(response.data));
  }, []);

  
  return (
    <ContentTableUser>
      <table>
        <thead>
          <tr>
            <th><FontAwesomeIcon icon="user" /> Nome</th>
            <th><FontAwesomeIcon icon="at" /> Email</th>
            <th><FontAwesomeIcon icon="phone" /> Celular</th>
            <th><FontAwesomeIcon icon="phone" /> Cargo</th>
          </tr>
        </thead>
        <tbody>
          {listUsers.map(user => {
            return (
              <tr key={user.admin_id} >
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.telefone}</td>
                <td>{user.cargo}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </ContentTableUser>
  );
}
