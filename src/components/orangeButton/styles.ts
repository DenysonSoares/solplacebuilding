import styled from "styled-components";
import { Colors } from "../../../styles/variables"

export const ButtonPrimary = styled.button`
  background-color: ${Colors.orangeDefault};
  padding: 10px 20px;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  border: 0;
  color: #ffffff;
  font-size: 18px;
  svg {
    margin-right: 10px;
  }
`