import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { ButtonPrimary } from "./styles"
library.add(fas);

export function OrangeButton(props){
  return(
    <ButtonPrimary type="submit" onClick={props.handleClick}><FontAwesomeIcon icon={props.iconname} />{props.buttontitle}</ButtonPrimary>
  );
}