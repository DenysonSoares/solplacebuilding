import router from "next/router"
import { useAuth } from "../../contexts/AuthProvider/useAuth";
import StartLogin from "../../pages/login";

export default function ProtectedProfile({children}: {children: JSX.Element}){
  const auth = useAuth();

  if (!auth.name){
    return StartLogin();
  }
  
  return children;
}