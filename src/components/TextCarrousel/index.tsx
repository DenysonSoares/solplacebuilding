import { Carrousel } from "./styles";

export function TextCarrousel() {
  return (
    <Carrousel>
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna
              nulla enim semper augue condimentum quis mattis.
            </p>
          </div>
          <div className="carousel-item">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna
              nulla enim semper augue condimentum quis mattis.
            </p>
          </div>
          <div className="carousel-item">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna
              nulla enim semper augue condimentum quis mattis.
            </p>
          </div>
        </div>
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
      </div>
    </Carrousel>
  );
}
