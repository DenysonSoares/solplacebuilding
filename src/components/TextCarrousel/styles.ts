import styled from "styled-components";

export const Carrousel = styled.div`
  border: 1px solid #ffffff;
  border-radius: 26px;
  background: linear-gradient(
    248.54deg,
    rgba(255, 255, 255, 0.1088) 1.47%,
    rgba(255, 255, 255, 0.0512) 100%
  );
  backdrop-filter: blur(30px);
  padding: 40px 30px;
  p {
    color: #ffffff;
    font-size: 18px;
    padding: 20px;
  }
  .carousel {
    display: flex;
    .carousel-indicators {
      position: relative;
      max-width: 50px;
      margin: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      button {
        width: 8px;
        height: 8px;
        border-radius: 50%;
        &.active {
          width: 15px;
          height: 15px;
        }
      }
    }
  }
`;
