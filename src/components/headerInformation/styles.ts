import styled from "styled-components";
import { Colors, Paddings, Fonts} from "../../../styles/variables";

export const ContenInformations = styled.div`
  background-color: #eff1fe;
  padding: ${Paddings.paddingContainer};
  position: relative;
  height: max-content;
  button{
    margin-bottom: 20px;
    margin-left: -65px;
    margin-top: -50px;
  }
  
`;

export const InformationDate = styled.div`
  margin-top: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 16px;
  p{
    font-size: 16px;
  }
`;
