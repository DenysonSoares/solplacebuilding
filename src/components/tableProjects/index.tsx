import { ContentTableProject } from "./styles"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon  } from "@fortawesome/react-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons'
import Link from "next/link";
import { useContext, useEffect, useMemo, useState } from "react";
import api  from "../../services/api";
import { AuthContext } from "../../contexts/AuthProvider/AuthContext";

library.add(fas);

export function TableProjects(props){
  const user = useContext(AuthContext);
  const [basicprojects, setBasicProjects ] = useState();
  const listProjects = useMemo( () => {
    // @ts-ignore
    return basicprojects?.result?.ok || []
  }, [basicprojects])
  
  useEffect(() => {
    api
      .post("/solplace/project/get",{
        params:{
          integrador_id: user.admin_id
        } 
      })
      .then(response => setBasicProjects(response.data));
  }, []);

  return (
    <ContentTableProject>
      <table>
        <thead>
          <tr>
            <th><FontAwesomeIcon icon="calendar" /> Criado em</th>
            <th><FontAwesomeIcon icon="at" /> Projeto</th>
            <th><FontAwesomeIcon icon="user" /> Cliente</th>
            <th><FontAwesomeIcon icon="user" /> Vendedor</th>
            <th><FontAwesomeIcon icon="user" /> Valor previsto</th>
            <th><FontAwesomeIcon icon="user" /> Último status criado</th>
            <th><FontAwesomeIcon icon="user" /> Ações</th>
          </tr>
        </thead>
        <tbody>
          {listProjects.map(project => {
            return ( 
              <tr key={project.partner_id}>
                <td>{project.criado_em}</td>
                <td>{project.nome_tecnico}</td>
                <td>{project.name}</td>
                <td>Antonio Luiz Gil Mendes</td>
                <td>{project.valor_previsto}</td>
                <td>Aguardando validações das notas fiscais.</td>
                <td><Link href={`projetos/${project.id}/`}><a><button><FontAwesomeIcon icon="eye" />Ver projeto</button></a></Link></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </ContentTableProject>
  );
}
