import styled from "styled-components";
import { Colors, Paddings } from "../../../styles/variables";

export const ContentTableProject = styled.main`
  padding: ${Paddings.paddingContainer};
  table {
    width: 100%;
    border-spacing: 0 0.5rem;
    thead {
      background-color: #fefefe;
      color: #575757;
      box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
      border-collapse: collapse;
      overflow: hidden;
      -moz-border-radius: 3px;
      -webkit-border-radius: 3px;
      border-radius: 3px;
      tr {
        border-radius: 10px;
        th {
          padding: 20px 30px;
          text-align: left;
          font-size: 16px;
          font-weight: 400;
        }
        :first-child {
          th:first-child {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
          }
          th:last-child {
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
          }
        }
      }
    }
    tbody {
      tr {
        background-color: #fefefe;
        border-radius: 8px;
        td {
          font-size: 16px;
          font-weight: 400;
          padding: 1rem 2rem;
          border: 0;
          :first-child {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
          }
          :last-child {
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
          }
          button {
            background-color: ${Colors.orangeDefault};
            padding: 10px 20px;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            border: 0;
            color: #ffffff;
            font-size: 18px;
          }
        }
      }
    }
  }
`;
