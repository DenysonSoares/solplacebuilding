import styled from "styled-components";
import { Colors } from "../../../styles/variables";

export const WrapperPresentationLogin = styled.div`
  height: 100vh;
  overflow: hidden;
  display: grid;
  grid-template-columns: 1fr 1fr;
`;


// Styles Presentations
export const WrapperPresentation = styled.div`
  background-color: ${Colors.blueDefault};
  padding: 10%;
  background: url("/images/bg-presentation.png") ${Colors.blueDefault} no-repeat;
  background-position: top right;
  background-size: cover;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  h1 {
    font-size: 42px;
    font-weight: 700;
    position: relative;
    padding-left: 5%;
    color: #ffffff;
    max-width: 500px;
    :before {
      content: "";
      background: url("/images/arrow-title.png");
      width: 23px;
      height: 29px;
      position: absolute;
      left: 0;
      top: 10px;
    }
  }
`;

export const ContainerLogin = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  background-color: #e5e5e5;
  padding-top: 7%;
  position: relative;
  img {
    position: relative;
    display: block;
    margin-top: -20%;
  }
  form {
    background-color: #fefefe;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.12);
    padding: 30px 80px;
    min-width: 500px;
    margin-top: 38px;
    h1{
      font-size: 30px;
      color: #575757;
      text-align: center;
    }
    label {
      width: 100%;
      &:last-child {
        display: flex;
        align-items: center;
        justify-content: space-between;
        text-align: left;
        a {
          display: block;
          float: right;
        }
      }
      input {
        width: 100%;
        height: 56px;
        border: 1px solid #e8e8e8;
        border-radius: 5px;
        &:last-child {
          margin-bottom: 25px;
        }
        &[type="submit"] {
          background-color: ${Colors.orangeDefault};
          color: #ffffff;
        }
        &[type="checkbox"] {
          width: 15px;
          float: left;
        }
      }
    }
  }
`;

export const LoginTitles = styled.h1`
  font-size: 30px;
  color: #575757;
  text-align: center;
  margin-top: 15%;
`;
