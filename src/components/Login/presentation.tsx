import { TextCarrousel } from "../TextCarrousel";
import { WrapperPresentation } from "./styles";

export function Presentation(props) {
  return (
    <WrapperPresentation>
      <h1>Viabilidade e confiança para o seu negócio</h1>
      <TextCarrousel />
    </WrapperPresentation>
  );
}
