import { ContainerLogin, LoginTitles } from "./styles";
import { useForm } from "react-hook-form";
import { useContext } from "react";
import Router from "next/router"
import Image from "next/image";
import Link from "next/link";
import  LogoSolPlace  from "../../../public/images/logo-solplace.png";
import { useAuth } from "../../contexts/AuthProvider/useAuth";
import router from "next/router";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Formik, Form, Field } from 'formik';

export function Login() {
  const auth = useAuth();
  
  const loginValidate = yup.object().shape({
    user: yup.string().email().required(),
    password: yup.string().required()
  })

  // const { register, handleSubmit, formState:{ errors }} = useForm({
  //   resolver: yupResolver(schema),
  // });

  // async function handleSignIn(data) {
  //   await auth.authenticate(data.user, data.password);  
  // }

  return (
    <ContainerLogin>
      <Image src={LogoSolPlace} alt="" />
      <LoginTitles>Controle de Acesso</LoginTitles>
      <Formik
        validationSchema={loginValidate}
        initialValues={{
          user: '',
          password: '',
        }}
        onSubmit={async (values) => {
          await auth.authenticate(values.user, values.password);
        }}
      >
        {({ errors }) => (
          <Form>
             <label htmlFor="user">
                E-mail
                <Field type="text" id="user" name="user" />
                {errors.user && (<span>Email invalido</span>)}
              </label>
              <label htmlFor="password">
                Senha
                <Field type="password" id="password" name="password"/>
                {errors.password && (<span>Senha Invalida</span>)}
              </label>
              <label>
                <input type="submit" value="Entrar" />
              </label>
          </Form>
        )}
      </Formik>

      {/* <form onSubmit={handleSubmit(handleSignIn)}>
        <h1>Login</h1>
        <label>
          E-mail
          <input {...register("user")} type="text" id="user" name="user" />
          <p>{errors.user && (<span>Email invalido</span>)}</p>
        </label>
        <label>
          Senha
          <input {...register("password")} type="password" />
          <p>{errors.password && (<span>Senha Invalida</span>)}</p>
        </label>
        <label>
          <input type="submit" value="Entrar" />
        </label>
        <label>
          <input type="checkbox" />
          Mantenha-me conectado
          <Link href="#">Esqueci minha senha</Link>
        </label>
      </form> */}
    </ContainerLogin>
  );
}
