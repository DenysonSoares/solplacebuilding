import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { FormWizard, NavigationSteps } from "./styles";

export default function WizardForm(){
  const [formStep, setFormStep] = useState(0);
  function NextStepForm(){
    setFormStep(cur => cur + 1);
  }
  function PrevStepForm(){
    setFormStep(cur => cur - 1);
  }
  return(
    <div>
      <FormWizard action="">
        {formStep == 0 && (
          <label>
            Razão Social
            <input type="text" />
            <FontAwesomeIcon icon="user" />
          </label>
        )}
        {formStep == 1 && (
          <label>
            CEP
            <input type="text" />
            <FontAwesomeIcon icon="user" />
          </label>
        )}
        {formStep == 2 && (
          <label>
            Banco
            <input type="text" />
            <FontAwesomeIcon icon="user" />
          </label>
        )}
        {formStep == 3 && (
          <label>
            Nome do Responsavel
            <input type="text" />
            <FontAwesomeIcon icon="user" />
          </label>
        )}
        {formStep == 4 && (
          <label>
            Numero das instalações realizadas
            <input type="text" />
            <FontAwesomeIcon icon="user" />
          </label>
        )}
        {formStep == 5 && (
          <h1>Parabéns</h1>
        )}
      </FormWizard>
      <NavigationSteps>
        {formStep > 0 && (
          <button className="prevStep" onClick={PrevStepForm}>Voltar</button>
        )}
        {formStep < 5 && (
          <button className="nextStep" onClick={NextStepForm}>Continuar</button>
        )}
        {formStep == 5 && (
          <button className="nextStep">Atualizar</button>
        )}
      </NavigationSteps>
    </div>
  );
}
