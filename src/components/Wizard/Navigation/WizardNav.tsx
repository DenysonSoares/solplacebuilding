import { useState } from "react";
import { ButtonChecked, NavButtons } from "./styles";
import WizardForm from "./WizardForm";

export default function WizardNavigation(){
  const [instep, setInstep] = useState("noActive");

  return(
    <div>
      <NavButtons>
        <ButtonChecked className={instep}>1</ButtonChecked>
        <ButtonChecked className={instep}>2</ButtonChecked>
        <ButtonChecked className={instep}>3</ButtonChecked>
        <ButtonChecked className={instep}>4</ButtonChecked>
        <ButtonChecked className={instep}>5</ButtonChecked>
        <ButtonChecked className={instep}>6</ButtonChecked>
      </NavButtons>
    </div>
  );
}
